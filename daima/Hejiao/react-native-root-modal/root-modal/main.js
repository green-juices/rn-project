/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {StyleSheet, Text, View, Button} from 'react-native';
// import {
//   Router,
//   routerReducer,
//   Route,
//   Animations,
//   Schema,
// } from 'react-native-redux-router';
import {Component} from 'react';
// import NavBar from './react-native-navbar/NavBar';
import SimpleModal from './SimpleModal';
import CustomStyle from './CustomStyle';
// import Animation from './Animation';
// import Launch from './Launch';
// import {legacy_createStore as createStore, combineReducers} from 'redux';
// import {Provider} from 'react-redux';

export default class RootModalExample extends Component {
  reader() {
    return (
      <View style={styles.container}>
        <Button
          title="SimpleModal"
          style={{
            textAlign: 'center',
            color: '#333333',
            marginBottom: 5,
          }}
          onPress={SimpleModal}
        />
        <Button
          title="CustomStyle"
          style={{
            textAlign: 'center',
            color: '#333333',
            marginBottom: 5,
          }}
          onPress={CustomStyle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
// export default RootModalExample;
