import { AMap,
  BitmapDescriptor,
  CameraPosition,
  CircleHoleOptions,
  CircleOptions,
  LatLng,
  MapsInitializer, MapView, MapViewComponent, MapViewManager, MarkerOptions,
  OnCameraChangeListener,
  PolygonHoleOptions,
  PolygonOptions,
  PolylineOptions,
  Marker,
} from '@amap/amap_lbs_map3d'
import {LogUtil} from '@amap/amap_lbs_common'
import ArrayList from '@ohos.util.ArrayList';

interface IMapController {
  show() : void;
  hide() : void;
  getAMap() : AMap | undefined;
  getMapView() : MapView | undefined;
  getId() : number;
}

class IdGenerator {
  static aId : number = 0;
  static aInstance : IdGenerator | undefined = undefined;

  static instance() : IdGenerator {
    if (!IdGenerator.aInstance) {
      IdGenerator.aInstance = new IdGenerator();
    }

    return IdGenerator.aInstance;
  }
  generateId() : number {
    return IdGenerator.aId++;
  }
}

class MapShowController implements IMapController {
  private mapView? : MapView;
  private aMap? : AMap;
  private aId : number;

  constructor() {
    this.aId = IdGenerator.instance().generateId();
  }

  getMapView(): MapView | undefined {
    return this.mapView;
  }

  getAMap(): AMap | undefined {
    return this.aMap;
  }

  getId() : number {
    return this.aId;
  }

  show() : void {
    MapsInitializer.setDebugMode(true);
    MapsInitializer.setApiKey('01b981a97c961158227ce3b8d1a069e5');

    MapViewManager.getInstance().getMapView((mapview? : MapView) => {
      if (!mapview) {
        return;
      }

      this.mapView = mapview;

      this.mapView.onCreate();
      this.mapView.getMapAsync((map) => {
        this.aMap = map;

        LogUtil.i('map3d', 'EntryAbility', 'get map ' + this.aMap?.getCameraPosition()?.zoom);

        let uisettings = this.aMap?.getUiSettings();
        let projection = this.aMap?.getProjection();


        this.aMap?.setOnMapClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map click ${point.longitude} ${point.latitude}`);
        });

        this.aMap?.setOnCameraChangeListener(new OnCameraChangeListener(
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map change ${position.target.longitude} ${position.target.latitude}`);
          },
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map finish ${position.target.longitude} ${position.target.latitude}`);
          }));

        this.aMap?.setOnMapTouchListener((event : TouchEvent) => {
          LogUtil.i('map3d', 'EntryAbility', `map touch ${event.touches[0].x} ${event.touches[0].y}`);
        });

        this.aMap?.setOnMapLongClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map long click ${point.longitude} ${point.latitude}`);
        });

        let approvalNumber = this.aMap?.getMapContentApprovalNumber();
        let satelliteImageApprovalNumber = this.aMap?.getSatelliteImageApprovalNumber();
        LogUtil.i('map3d', 'EntryAbility', `getMapContentApprovalNumber ${approvalNumber} satelliteImageApprovalNumber ${satelliteImageApprovalNumber}` );
      })

    });
  }

  hide() : void {
    if (this.mapView) {
      this.mapView.onDestroy();
    }
  }
}


class MapMarkerController implements IMapController {
  private mapView? : MapView;
  private aMap? : AMap;
  private aId : number;

  constructor() {
    this.aId = IdGenerator.instance().generateId();

  }

  getMapView(): MapView | undefined {
    return this.mapView;
  }

  getAMap(): AMap | undefined {
    return this.aMap;
  }

  getId(): number {
    return this.aId;
  }

  show() : void {
    MapsInitializer.setDebugMode(true);
    MapsInitializer.setApiKey('01b981a97c961158227ce3b8d1a069e5');

    MapViewManager.getInstance().getMapView((mapview? : MapView) => {
      if (!mapview) {
        return;
      }

      this.mapView = mapview;

      this.mapView.onCreate();
      this.mapView.getMapAsync((map) => {
        this.aMap = map;

        LogUtil.i('map3d', 'EntryAbility', 'get map ' + this.aMap?.getCameraPosition()?.zoom);

        let uisettings = this.aMap?.getUiSettings();
        let projection = this.aMap?.getProjection();


        this.aMap?.setOnMapClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map click ${point.longitude} ${point.latitude}`);
        });

        this.aMap?.setOnCameraChangeListener(new OnCameraChangeListener(
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map change ${position.target.longitude} ${position.target.latitude}`);
          },
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map finish ${position.target.longitude} ${position.target.latitude}`);
          }));

        this.aMap?.setOnMapTouchListener((event : TouchEvent) => {
          LogUtil.i('map3d', 'EntryAbility', `map touch ${event.touches[0].x} ${event.touches[0].y}`);
        });

        this.aMap?.setOnMapLongClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map long click ${point.longitude} ${point.latitude}`);
        });

        // add Marker
        let options: MarkerOptions = new MarkerOptions();
        options.setPosition(new LatLng(39.992520, 116.336170));
        options.setAnchor(0.5, 1.0);
        // options.setPosition(new LatLng(40.02380181476392, 116.43124537956452));
        options.setTitle('Anno');
        options.setSnippet('111111111')
        // options.setVisible(false);
        // options.setIcon(new BitmapDescriptor($rawfile('location_map_gps_locked.png'), 'location_map_gps_locked', 100, 100));
        let marker = this.aMap?.addMarker(options);
        if (marker) {
          marker.showInfoWindow();
          // this.aMap.removeOverlay(marker.getId());
          // marker.setVisible(true);
          marker.setIcon(new BitmapDescriptor($rawfile('location_map_gps_locked.png'), 'location_map_gps_locked', 128, 128));
          // marker.setPosition(new LatLng(40.02380181476392, 116.43124537956452));
          marker.setDraggable(true);
        }

        this.aMap?.setOnMarkerClickListener((marker : Marker):boolean => {
          LogUtil.i('map3d', 'EntryAbility', `marker click ${marker.getId()} ${marker.getTitle()}`);
          return true;
        });
      })

    });
  }

  hide() : void{
    if (this.mapView) {
      this.mapView.onDestroy();
    }
  }
}


class MapPolylineController implements IMapController {
  private mapView? : MapView;
  private aMap? : AMap;
  private aId : number;

  constructor() {
    this.aId = IdGenerator.instance().generateId();
  }

  getMapView(): MapView | undefined {
    return this.mapView;
  }

  getAMap(): AMap | undefined {
    return this.aMap;
  }

  getId(): number {
    return this.aId;
  }

  show() : void {
    MapsInitializer.setDebugMode(true);
    MapsInitializer.setApiKey('01b981a97c961158227ce3b8d1a069e5');

    MapViewManager.getInstance().getMapView((mapview? : MapView) => {
      if (!mapview) {
        return;
      }

      this.mapView = mapview;

      this.mapView.onCreate();
      this.mapView.getMapAsync((map) => {
        this.aMap = map;

        LogUtil.i('map3d', 'EntryAbility', 'get map ' + this.aMap?.getCameraPosition()?.zoom);

        let uisettings = this.aMap?.getUiSettings();
        let projection = this.aMap?.getProjection();


        this.aMap?.setOnMapClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map click ${point.longitude} ${point.latitude}`);
        });

        this.aMap?.setOnCameraChangeListener(new OnCameraChangeListener(
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map change ${position.target.longitude} ${position.target.latitude}`);
          },
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map finish ${position.target.longitude} ${position.target.latitude}`);
          }));

        this.aMap?.setOnMapTouchListener((event : TouchEvent) => {
          LogUtil.i('map3d', 'EntryAbility', `map touch ${event.touches[0].x} ${event.touches[0].y}`);
        });

        this.aMap?.setOnMapLongClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map long click ${point.longitude} ${point.latitude}`);
        });

        // add polyline
        let options: PolylineOptions = new PolylineOptions();
        options.add(new LatLng(39.925539, 116.279037));
        options.add(new LatLng(39.925539, 116.520285));
        this.aMap?.addPolyline(options);
      })

    });
  }

  hide() : void {
    if (this.mapView) {
      this.mapView.onDestroy();
    }
  }
}

class MapPolygonController implements IMapController {
  private mapView? : MapView;
  private aMap? : AMap;
  private aId : number;

  constructor() {
    this.aId = IdGenerator.instance().generateId();
  }

  getMapView(): MapView | undefined {
    return this.mapView;
  }

  getAMap(): AMap | undefined {
    return this.aMap;
  }

  getId(): number {
    return this.aId;
  }

  show() : void {
    MapsInitializer.setDebugMode(true);
    MapsInitializer.setApiKey('01b981a97c961158227ce3b8d1a069e5');

    MapViewManager.getInstance().getMapView((mapview? : MapView) => {
      if (!mapview) {
        return;
      }

      this.mapView = mapview;

      this.mapView.onCreate();
      this.mapView.getMapAsync((map) => {
        this.aMap = map;

        LogUtil.i('map3d', 'EntryAbility', 'get map ' + this.aMap?.getCameraPosition()?.zoom);

        let uisettings = this.aMap?.getUiSettings();
        let projection = this.aMap?.getProjection();


        this.aMap?.setOnMapClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map click ${point.longitude} ${point.latitude}`);
        });

        this.aMap?.setOnCameraChangeListener(new OnCameraChangeListener(
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map change ${position.target.longitude} ${position.target.latitude}`);
          },
          (position : CameraPosition) => {
            LogUtil.i('map3d', 'EntryAbility', `map finish ${position.target.longitude} ${position.target.latitude}`);
          }));

        this.aMap?.setOnMapTouchListener((event : TouchEvent) => {
          LogUtil.i('map3d', 'EntryAbility', `map touch ${event.touches[0].x} ${event.touches[0].y}`);
        });

        this.aMap?.setOnMapLongClickListener((point : LatLng) => {
          LogUtil.i('map3d', 'EntryAbility', `map long click ${point.longitude} ${point.latitude}`);
        });

        // add Polygon
        let polygonOptions: PolygonOptions = new PolygonOptions();
        polygonOptions.add(new LatLng(39.781892, 116.293413));
        polygonOptions.add(new LatLng(39.787600, 116.391842));
        polygonOptions.add(new LatLng(39.733187, 116.417932));
        polygonOptions.add(new LatLng(39.704653, 116.338255));
        polygonOptions.setFillColor(0xFFFF0000);

        let polygonHoleOptions: PolygonHoleOptions = new PolygonHoleOptions;
        polygonHoleOptions.add(new LatLng(39.781892 - 0.02, 116.293413 + 0.02));
        polygonHoleOptions.add(new LatLng(39.787600 -0.02, 116.391842 - 0.02));
        polygonHoleOptions.add(new LatLng(39.733187 + 0.02, 116.417932 - 0.02));
        polygonHoleOptions.add(new LatLng(39.704653 + 0.02, 116.338255 + 0.02));
        let list: ArrayList<PolygonHoleOptions> = new ArrayList();
        list.add(polygonHoleOptions);
        polygonOptions.setHoleOptions(list);
        this.aMap.addPolygon(polygonOptions);

        // add Circle
        let circleOptions: CircleOptions = new CircleOptions();
        circleOptions.setRadius(15000);
        circleOptions.setCenter(new LatLng(39.996441, 116.411146));
        circleOptions.setFillColor(0xffff0000)
        circleOptions.setStrokeColor(0xff00ff00)

        let circleHoleOptions: CircleHoleOptions = new CircleHoleOptions();
        circleHoleOptions.setRadius(5000);
        circleHoleOptions.setCenter(new LatLng(39.996441, 116.411146));
        circleOptions.addHoles(circleHoleOptions);
        this.aMap.addCircle(circleOptions)
      })

    });
  }

  hide() : void {
    if (this.mapView) {
      this.mapView.onDestroy();
    }
  }
}


const MAP_SHOW_FUNC = '显示地图';
const MAP_MARKER_FUNC = 'Marker功能';
const MAP_POLYLINE_FUNC = 'Polyline功能';
const MAP_POLYGON_FUNC = 'Polygon功能';

class ListItemContent {
  title : string = '';
  info : string = '';
  mapController? : IMapController;
  state : boolean = false;
}

@Component
export struct RNMapView {
  @State showMapStates : boolean[] = [true, true, true, true];
  //显示地图
  @Builder
  buildMapShow(index :number) {
    Column() {
      if (this.showMapStates[index]) {
        MapViewComponent()
      }
    }.width('100%')
    .height('100%')
  }

  //marker功能
  @Builder
  buildMapMarker(index :number) {
    Column() {
      if (this.showMapStates[index]) {
        MapViewComponent()
      }
    }.width('100%')
    .height('100%')
  }

  //polyline功能
  @Builder
  buildMapPolyline(index :number) {
    Column() {
      if (this.showMapStates[index]) {
        MapViewComponent()
      }
    }.width('100%')
    .height('100%')
  }

  //polygon功能
  @Builder
  buildMapPolygon(index :number) {
    Column() {
      if (this.showMapStates[index]) {
        MapViewComponent()
      }
    }.width('100%')
    .height('100%')
  }


  @Builder
  buildMap(title : string, index : number) {
    if(title == MAP_SHOW_FUNC) {
      this.buildMapShow(index);
    } else if (title == MAP_MARKER_FUNC) {
      this.buildMapMarker(index);
    } else if(title == MAP_POLYLINE_FUNC) {
      this.buildMapPolyline(index);
    } else if (title == MAP_POLYGON_FUNC) {
      this.buildMapPolygon(index);
    }
  }

  private pages : ListItemContent[] = [
    {title : MAP_SHOW_FUNC, info : '介绍创建一个地图', mapController : new MapShowController(), state : false},
    {title : MAP_MARKER_FUNC, info : '介绍Marker功能', mapController : new MapMarkerController(), state : false},
    {title : MAP_POLYLINE_FUNC, info : '介绍线功能', mapController : new MapPolylineController(), state : false},
    {title : MAP_POLYGON_FUNC, info : '介绍面功能', mapController : new MapPolygonController(), state : false},
  ];

  build() {
    Column() {
      Navigation() {

        List() {
          ForEach(this.pages, (content : ListItemContent, index : number)=> {
            ListItem() {
              NavRouter() {
                Column() {
                  Text(content.title)
                    .width("100%")
                    .height(50)
                    .backgroundColor('#FFFFFF')
                    .borderRadius(24)
                    .fontSize(16)
                    .fontWeight(50)
                    .textAlign(TextAlign.Start)
                    .padding( {
                      left: 20
                    })
                  Text(content.info)
                    .width('100%')
                    .height(20)
                    .fontSize(12)
                    .textAlign(TextAlign.Start)
                    .fontColor(Color.Gray)
                    .padding( {
                      left: 20
                    })
                }
                NavDestination() {
                  this.buildMap(content.title, index);
                }
                .title(content.title)
                .onShown(() => {
                  LogUtil.i('map3d', 'EntryAbility', "map show " + content.mapController?.getId());
                  // this.showMap = true;
                  this.showMapStates[index] = true;
                  content.mapController?.show();
                })
                .onHidden(()=> {
                  LogUtil.i('map3d', 'EntryAbility', "map hide " + content.mapController?.getId());
                  content.mapController?.hide();
                  this.showMapStates[index] = false;
                  // this.showMap = false;
                })
              }

            }
          })
        }.width('100%')
        .height('100%')
        .divider({
          strokeWidth: 0.5,
          color: Color.Black
        })
      }.title('地图示例')
      .titleMode(NavigationTitleMode.Mini)
      .mode(NavigationMode.Auto)
    }
  }
}

