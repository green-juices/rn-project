#pragma once

#include "RNOH/arkui/ArkUINode.h"

namespace rnoh {

class CircleStackNode : public ArkUINode {
public:
    CircleStackNode();

    void insertChild(ArkUINode &child, std::size_t index);
    void removeChild(ArkUINode &child);

    //CircleStackNode &setCircle(int const &);
    CircleStackNode &setRadius(const float &);
    CircleStackNode &setStrokeWidth(const float &);
    CircleStackNode &setStrokeColor(const int &);
    CircleStackNode &setFillColor(const int &);
    CircleStackNode &setLevelIndex(const int  &);
};

}