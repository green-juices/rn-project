#include "MultipointStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    MultipointStackNode::MultipointStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void MultipointStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->addChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    void MultipointStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    MultipointStackNode &MultipointStackNode::setItems(const std::multimap<float, float> & items){
        for (auto it = items.begin(); it != items.end(); it = items.upper_bound(it->first)) {
            ArkUI_NumberValue indexValue[] = {{.f32 = it->first}, {.f32 = it->second}};
            ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
            maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_POSITION, &indexItem));
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->first:" << indexItem.value[0].f32;
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->second:" << indexItem.value[1].f32;
        }
    }
    MultipointStackNode &MultipointStackNode::setIcon(const std::multimap<std::string, std::string> & icon){
        for (auto it = icon.begin(); it != icon.end(); it = icon.upper_bound(it->first)) {
            ArkUI_AttributeItem indexItem = {.string = it->first.c_str(),.string = it->second.c_str()};
            //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_POSITION, &indexItem));
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->first:" << indexItem.value[0].f32;
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->second:" << indexItem.value[1].f32;
        }
    }

} // namespace rnoh