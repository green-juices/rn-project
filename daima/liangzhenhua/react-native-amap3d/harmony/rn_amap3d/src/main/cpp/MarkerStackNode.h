#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include "RNOH/arkui/StackNode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

class MarkerStackNode : public ArkUINode {
public:
    MarkerStackNode();

    void insertChild(ArkUINode &child, std::size_t index);
    void removeChild(ArkUINode &child);

    //MarkerStackNode &setMarker(int const &);
    MarkerStackNode &setOpacity(const float  &);
    MarkerStackNode &setDraggable(const bool&);
    MarkerStackNode &setFlat(const bool&);
    MarkerStackNode &setLevelIndex(const int  &);
};

} // namespace rnoh