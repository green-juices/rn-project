#ifndef HARMONY_MAPVIEWCOMPONENTINSTANCE_H
#define HARMONY_MAPVIEWCOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "MapViewStackNode.h"
#include "Props.h"
#include "ShadowNodes.h"

namespace rnoh {
    class MapViewComponentInstance : public CppComponentInstance<facebook::react::MapViewShadowNode> {
    private:
        MapViewStackNode m_stackNode;

        // 初始状态
        std::multimap<float, float> initialCameraPosition;
        // 是否显示当前定位
        bool myLocationEnabled;
        // 是否显示室内地图
        bool indoorViewEnabled;
        // 是否显示3D建筑
        bool buildingsEnabled;
        // 是否显示指南针
        bool compassEnabled;
        // 是否显示放大缩小按钮
        bool zoomControlsEnabled;
        // 是否显示比例尺
        bool scaleControlsEnabled;
        // 语言
        std::string language;
        // 是否显示定位按钮
        bool myLocationButtonEnabled;
        // 是否显示路况
        bool trafficEnabled;
        // 最大缩放级别
        float maxZoom;
        // 最小缩放级别
        float minZoom;
        // 地图类型
        int mapType;
        // 是否启用缩放手势，用于放大缩小
        bool zoomGesturesEnabled;
        // 是否启用滑动手势，用于平移
        bool scrollGesturesEnabled;
        // 是否启用旋转手势，用于调整方向
        bool rotateGesturesEnabled;
        // 是否启用倾斜手势，用于改变视角
        bool tiltGesturesEnabled;
        // 相机位置
        std::multimap<float, float> camerPosition;

    public:
        MapViewComponentInstance(Context context);
        void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
        void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
        MapViewStackNode &getLocalRootArkUINode() override;
        void onPropsChanged(SharedConcreteProps const &props) override;
        void getNapiProps(facebook::react::Props::Shared props);
    };
} // namespace rnoh

#endif 