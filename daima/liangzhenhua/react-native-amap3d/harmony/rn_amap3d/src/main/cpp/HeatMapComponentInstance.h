#ifndef HARMONY_HEATMAPCOMPONENTINSTANCE_H
#define HARMONY_HEATMAPCOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "RNOH/arkui/StackNode.h"
#include "HeatMapStackNode.h"
#include "ShadowNodes.h"

namespace rnoh {
    class HeatMapComponentInstance : public CppComponentInstance<facebook::react::HeatMapShadowNode> {
    private:
        HeatMapStackNode m_stackNode;

        // 节点坐标
        std::multimap<float, float> data;
        // 半径（米）
        float radius;
        // 透明度
        float opacity;

    public:
        HeatMapComponentInstance(Context context);
        void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
        void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
        HeatMapStackNode &getLocalRootArkUINode() override;
        void onPropsChanged(SharedConcreteProps const &props) override;
        void getNapiProps(facebook::react::Props::Shared props);
    };
} // namespace rnoh

#endif 