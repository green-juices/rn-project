#include "PolygonComponentInstance.h"
#include <glog/logging.h>
#include "PolygonStackNode.h"

namespace rnoh {
PolygonComponentInstance::PolygonComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void PolygonComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void PolygonComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

PolygonStackNode &PolygonComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void PolygonComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::PolygonProps>(props);
    if (p == nullptr) {
        LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> strokeWidth: ";
        return;
    } else {
        //LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> points: " << props->points;
        LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> setstrokeColor: " << props->strokeColor;
        LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> fillColor: " << props->fillColor;
        LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> strokeWidth: " << props->strokeWidth;
        LOG(INFO) << "[clx] <PolygonComponentInstance::setProps> levelIndex: " << props->levelIndex;

        this->points = p->points;
        this->strokeWidth = p->strokeWidth;
        this->strokeColor = p->strokeColor;
        this->fillColor = p->fillColor;
        this->levelIndex = p->levelIndex;

        this->getLocalRootArkUINode().setPoints(p->points);
        this->getLocalRootArkUINode().setStrokeWidth(p->strokeWidth);
        this->getLocalRootArkUINode().setStrokeColor(p->strokeColor);
        this->getLocalRootArkUINode().setFillColor(p->fillColor);
        this->getLocalRootArkUINode().setLevelIndex(p->levelIndex);
    }
}
void PolygonComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::PolygonProps>(props);
    this->points = p->points;
    this->strokeWidth = p->strokeWidth;
    this->strokeColor = p->strokeColor;
    this->fillColor = p->fillColor;
    this->levelIndex = p->levelIndex;
}

} // namespace rnoh