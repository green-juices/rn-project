#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

    class HeatMapStackNode : public ArkUINode {
    public:
        HeatMapStackNode();
        void insertChild(ArkUINode &child, std::size_t index);
        void removeChild(ArkUINode &child);

        HeatMapStackNode &setRadius(const float &);
        HeatMapStackNode &setOpacity(const float &);
    };

} // namespace rnoh