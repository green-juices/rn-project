#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

class MultipointStackNode : public ArkUINode {
public:
    MultipointStackNode();

    void insertChild(ArkUINode &child, std::size_t index);
    void removeChild(ArkUINode &child);
    MultipointStackNode &setItems(const std::multimap<float, float>  &);
    MultipointStackNode &setIcon(const std::multimap<std::string, std::string> &);
};

} // namespace rnoh