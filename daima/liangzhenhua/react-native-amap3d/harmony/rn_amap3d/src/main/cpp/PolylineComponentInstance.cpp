#include "PolylineComponentInstance.h"
#include <glog/logging.h>

namespace rnoh {
PolylineComponentInstance::PolylineComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void PolylineComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void PolylineComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

PolylineStackNode &PolylineComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void PolylineComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto PolylineProps = std::dynamic_pointer_cast<const facebook::react::PolylineProps>(props);
    if (PolylineProps == nullptr) {
        return;
    }
    this->gradient = PolylineProps->gradient;
    this->width = PolylineProps->width;
    this->geodesic = PolylineProps->geodesic;
    this->dotted = PolylineProps->dotted;
    this->color = PolylineProps->color;
    this->colors = PolylineProps->colors;
    this->levelIndex = PolylineProps->levelIndex;
    this->points = PolylineProps->points;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> width: " << props->width;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> color: " << props->color;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> dotted: " << props->dotted;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> geodesic: " << props->geodesic;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> dotted: " << props->dotted;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> color: " << props->color;
    LOG(INFO) << "[clx] <PolylineComponentInstance::setProps> levelIndex: " << props->levelIndex;
	this->getLocalRootArkUINode().setWidth(props->width);
    this->getLocalRootArkUINode().setGradient(props->gradient);
    this->getLocalRootArkUINode().setGeodesic(props->geodesic);
    this->getLocalRootArkUINode().setDotted(props->dotted);
    this->getLocalRootArkUINode().setColor(props->color);
    this->getLocalRootArkUINode().setColors(props->colors);
    this->getLocalRootArkUINode().setLevelIndex(props->levelIndex);
    this->getLocalRootArkUINode().setPoints(props->points);
}
void PolylineComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    if(auto PolylineProps = std::dynamic_pointer_cast<const facebook::react::PolylineProps>(props)){
        this->gradient = PolylineProps->gradient;
        this->width = PolylineProps->width;
        this->geodesic = PolylineProps->geodesic;
        this->dotted = PolylineProps->dotted;
        this->color = PolylineProps->color;
        this->colors = PolylineProps->colors;
        this->levelIndex = PolylineProps->levelIndex;
        this->points = PolylineProps->points;
    }
}
} // namespace rnoh