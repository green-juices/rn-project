#ifndef HARMONY_MAPVIEW_SRC_MAIN_CPP_MAPVIEWPACKAGE_H
#define HARMONY_MAPVIEW_SRC_MAIN_CPP_MAPVIEWPACKAGE_H


#include "RNOH/Package.h"
#include "ComponentDescriptors.h"
#include "MapViewJSIBinder.h"

// #include "MapViewComponentInstance.h"
// #include "HeatMapComponentInstance.h"
// #include "CircleComponentInstance.h"
// #include "PolylineComponentInstance.h"
// #include "PolygonComponentInstance.h"
// #include "MultipointComponentInstance.h"
// #include "MarkerComponentInstance.h"

namespace rnoh{
	
// class MapViewPackageComponentInstanceFactoryDelegate : public ComponentInstanceFactoryDelegate {
// public:
//     using ComponentInstanceFactoryDelegate::ComponentInstanceFactoryDelegate;
//
//     ComponentInstance::Shared create(ComponentInstance::Context ctx) override {
//         LOG(INFO) << "[clx] <ComponentInstance ctx.componentName " << ctx.componentName << " ComponentInstance";
//         if (ctx.componentName == "RNMapView") {
//             return std::make_shared<MapViewComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNHeatMap") {
//             return std::make_shared<HeatMapComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNCircle") {
//             return std::make_shared<CircleComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNPolyline") {
//             return std::make_shared<PolylineComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNPolygon") {
//             return std::make_shared<PolygonComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNMultipoint") {
//             return std::make_shared<MultipointComponentInstance>(std::move(ctx));
//         } else if (ctx.componentName == "RNMarker") {
//             return std::make_shared<MarkerComponentInstance>(std::move(ctx));
//         }
//         return nullptr;
//     }
// };
// class RNMapViewContextFactoryDelegate : public TurboModuleFactoryDelegate{
//     public:
//     SharedTurboModule createTurboModule(Context ctx,const std::string &name) const override
//     {
//         if(name == "AMapSDK"){
//             return std::make_shared<AMapSDKTurboModule>(ctx,name);
//         }
//         return nullptr;
//     }
// };

class MapViewPackage : public Package {
public:
//     MapViewPackage(Package::Context ctx) : Package(ctx) {}
//     ComponentInstanceFactoryDelegate::Shared createComponentInstanceFactoryDelegate() override {
//         LOG(INFO) << "[clx] <ComponentInstanceFactoryDelegate";
//         return std::make_shared<MapViewPackageComponentInstanceFactoryDelegate>();
//     }

    std::vector<facebook::react::ComponentDescriptorProvider> createComponentDescriptorProviders() override {
        LOG(INFO) << "[clx] <ComponentDescriptorProvider";
        return {facebook::react::concreteComponentDescriptorProvider<facebook::react::MapViewComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::HeatMapComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::CircleComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::PolylineComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::PolygonComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::MultipointComponentDescriptor>(),
                facebook::react::concreteComponentDescriptorProvider<facebook::react::MarkerComponentDescriptor>()};
    }

    ComponentJSIBinderByString createComponentJSIBinderByName() override {
        LOG(INFO) << "[clx] <ComponentJSIBinderByString ComponentJSIBinderByString";
        return {{"AMapView", std::make_shared<MapViewJSIBinder>()},
                {"AMapHeatMap", std::make_shared<HeatMapJSIBinder>()},
                {"AMapCircle", std::make_shared<CircleJSIBinder>()},
                {"AMapPolyline", std::make_shared<PolylineJSIBinder>()},
                {"AMapPolygon", std::make_shared<PolygonJSIBinder>()},
                {"AMapMultiPoint", std::make_shared<MultipointJSIBinder>()},
                {"AMapMarker", std::make_shared<MarkerJSIBinder>()}};
    }
//     ComponentNapiBinderByString createComponentNapiBinderByName() override {
//         LOG(INFO) << "[clx] <ComponentNapiBinderByString ComponentNapiBinderByString";
//         return {{"RNMapView", std::make_shared<MapViewNapiBinder>()},
//                 {"RNHeatMap", std::make_shared<HeatMapNapiBinder>()},
//                 {"RNCircle", std::make_shared<CircleNapiBinder>()},
//                 {"RNPolyline", std::make_shared<PolylineNapiBinder>()},
//                 {"RNPolygon", std::make_shared<PolygonNapiBinder>()},
//                 {"RNMultipoint", std::make_shared<MultipointNapiBinder>()},
//                 {"RNMarker", std::make_shared<MarkerNapiBinder>()}};
//     }
    
};
} // namespace rnoh
#endif