#include "HeatMapComponentInstance.h"
#include <glog/logging.h>

namespace rnoh {
HeatMapComponentInstance::HeatMapComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void HeatMapComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void HeatMapComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

HeatMapStackNode &HeatMapComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void HeatMapComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::HeatMapProps>(props);
    if (p == nullptr) {
        return;
    }else {
        LOG(INFO) << "[clx] <CardViewComponentInstance::onPropsChanged> Failed to cast props to RNCardViewProps.";
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> radius: " << props->radius;
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> opacity: " << props->opacity;
        this->radius = p->radius;
        this->opacity = p->opacity;
        this->getLocalRootArkUINode().setRadius(p->radius);
        this->getLocalRootArkUINode().setOpacity(p->opacity);
    }
}
void HeatMapComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::HeatMapProps>(props);

    this->radius = p->radius;
    this->opacity = p->opacity;
}

} // namespace rnoh