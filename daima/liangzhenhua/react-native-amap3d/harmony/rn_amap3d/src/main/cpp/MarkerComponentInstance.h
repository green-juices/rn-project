#ifndef HARMONY_MARKERCOMPONENTINSTANCE_H
#define HARMONY_MARKERCOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "ShadowNodes.h"
#include "MarkerStackNode.h"

namespace rnoh {
class MarkerComponentInstance : public CppComponentInstance<facebook::react::MarkerShadowNode> {
private:
    MarkerStackNode m_stackNode;
    // 坐标
    std::multimap<float, float> position;
    // 透明度 [0, 1]
    float opacity;
    // 是否可拖拽
    bool draggable;
    // 是否平贴地图
    bool flat;
    // 层级
    int levelIndex;
    // 覆盖物锚点比例
    std::multimap<float, float> anchor;
    // icon
    std::multimap<float, float> icon;

public:
    MarkerComponentInstance(Context context);
    void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
    void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
    MarkerStackNode &getLocalRootArkUINode() override;
    void onPropsChanged(SharedConcreteProps const &props) override;
    void getNapiProps(facebook::react::Props::Shared props);
};
} // namespace rnoh

#endif