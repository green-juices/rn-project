#include "MultipointComponentInstance.h"
#include <glog/logging.h>

namespace rnoh {
MultipointComponentInstance::MultipointComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void MultipointComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void MultipointComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

MultipointStackNode &MultipointComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void MultipointComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::MultipointProps>(props);
    if (p == nullptr) {
        LOG(INFO) << "[clx] <MultipointComponentInstance::setProps> strokeWidth: ";
        return;
    } else {

    }
}
void MultipointComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::MultipointProps>(props);
}
} // namespace rnoh