#ifndef HARMONY_MULTIPOINTCOMPONENTINSTANCE_H
#define HARMONY_MULTIPOINTCOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "ShadowNodes.h"
#include "MultipointStackNode.h"

namespace rnoh {
class MultipointComponentInstance : public CppComponentInstance<facebook::react::MultipointShadowNode> {
private:
    MultipointStackNode m_stackNode;

    // 圆点坐标
     std::map<float, float> multipoint;
    // 半径
    float radius;
    // 边线宽度
    float strokeWidth;
    // 边线颜色
    std::string strokeColor;
    // 填充颜色
    std::string fillColor;
    // 层级
    int levelIndex;

public:
    MultipointComponentInstance(Context context);
    void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
    void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
    MultipointStackNode &getLocalRootArkUINode() override;
    void onPropsChanged(SharedConcreteProps const &props) override;
    void getNapiProps(facebook::react::Props::Shared props);
};
} // namespace rnoh

#endif