#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include "RNOH/arkui/StackNode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

class PolygonStackNode : public ArkUINode {
public:
    PolygonStackNode();

    void insertChild(ArkUINode &child, std::size_t index);
    void removeChild(ArkUINode &child);

    //PolygonStackNode &setPolygon(int const &);
    
    PolygonStackNode &setStrokeWidth(const float &);
    PolygonStackNode &setStrokeColor(const int &);
    PolygonStackNode &setFillColor(const int &);
    PolygonStackNode &setLevelIndex(const float  &);
    PolygonStackNode &setPoints(const  std::multimap<float,float>  &);
};

} // namespace rnoh