#include "MarkerComponentInstance.h"
#include <glog/logging.h>

namespace rnoh {
MarkerComponentInstance::MarkerComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void MarkerComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void MarkerComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

MarkerStackNode &MarkerComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void MarkerComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::MarkerProps>(props);
    if (p == nullptr) {
        LOG(INFO) << "[clx] <MarkerComponentInstance::setProps> strokeWidth: ";
        return;
    } else {
        LOG(INFO) << "[clx] <MarkerComponentInstance::setProps> opacity: " << props->opacity;
        LOG(INFO) << "[clx] <MarkerComponentInstance::setProps> draggable: " << props->draggable;
        LOG(INFO) << "[clx] <MarkerComponentInstance::setProps> flat: " << props->flat;
        LOG(INFO) << "[clx] <MarkerComponentInstance::setProps> levelIndex: " << props->levelIndex;
        
        this->getLocalRootArkUINode().setOpacity(p->opacity);
        this->getLocalRootArkUINode().setDraggable(p->draggable);
        this->getLocalRootArkUINode().setFlat(p->flat);
        this->getLocalRootArkUINode().setLevelIndex(p->levelIndex);
    }
}
void MarkerComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::MarkerProps>(props);

    this->opacity = p->opacity;
    this->draggable = p->draggable;
    this->flat = p->flat;
    this->levelIndex = p->levelIndex;
}

} // namespace rnoh