#include "PolylineStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    PolylineStackNode::PolylineStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void PolylineStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->insertChildAt(m_nodeHandle, child.getArkUINodeHandle(), index));
    }

    void PolylineStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }
PolylineStackNode &PolylineStackNode::setWidth(const float & width){
        ArkUI_NumberValue indexValue[] = {{.f32 = width}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::width> elevation:" << width;
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setGradient(const bool & gradient){
//         ArkUI_NumberValue indexValue[] = {,};
//         ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
//         maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_RADIAL_GRADIENT, &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::gradient> elevation:" << gradient;
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setGeodesic(const bool & geodesic){
        ArkUI_NumberValue indexValue[] = {{.i32 = geodesic}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, , &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::geodesic> elevation:" << geodesic;
        return *this;
    }
	PolylineStackNode &PolylineStackNode::setDotted(const bool & dotted){
        ArkUI_NumberValue indexValue[] = {{.i32 = dotted}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle,NODE_DOTT , &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::dotted> elevation:" << dotted;
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setColor(const int &color){
        ArkUI_NumberValue indexValue[] = {{.i32 = color}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::color> elevation:" << color;
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setColors(const std::vector<int> & colors){
        for(int i = 0;i<colors.size();i++){
            ArkUI_NumberValue indexValue[] = {{.i32 = colors[i]}};
            ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
            maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
            LOG(INFO) << "[clx] <PolylineStackNode::colors> elevation:" << colors[i];
        }
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setLevelIndex(const float & levelIndex){
        ArkUI_NumberValue indexValue[] = {{.f32 = levelIndex}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolylineStackNode::levelIndex> elevation:" <<levelIndex;
        return *this;
    }
    PolylineStackNode &PolylineStackNode::setPoints(const std::multimap<float, float> & points){
        for (auto it = points.begin(); it != points.end(); it = points.upper_bound(it->first)){
            ArkUI_NumberValue indexValue[] = {{.f32 = it->first},{.f32 = it->second}};
            ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
            maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_POSITION, &indexItem));
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->first:" << indexItem.value[0].f32;
            LOG(INFO) << "[clx] <PolylineStackNode::setPoints> it->second:" << indexItem.value[1].f32;
        }
        return *this;
    }
} // namespace rnoh