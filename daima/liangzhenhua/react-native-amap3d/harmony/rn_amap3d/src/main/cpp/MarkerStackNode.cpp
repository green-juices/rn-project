#include "MarkerStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    MarkerStackNode::MarkerStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void MarkerStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->addChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    void MarkerStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    MarkerStackNode &MarkerStackNode::setOpacity(const float  &opacity) {
        ArkUI_NumberValue indexValue[] = {{.f32 = opacity}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        LOG(INFO) << "[clx] <MarkerStackNode::setCornerRadius> opacity: " << opacity;
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_OPACITY, &indexItem));
        return *this;
    }
    MarkerStackNode &MarkerStackNode::setDraggable(const bool &draggable) {
        if (!draggable) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(draggable)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_IMAGE_DRAGGABLE, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeWidth> draggable:" << draggable;
        return *this;
    }
	 MarkerStackNode &MarkerStackNode::setFlat(const bool &flat) {
        if (!flat) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(flat)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> flat:" << flat;
        return *this;
    }
    MarkerStackNode &MarkerStackNode::setLevelIndex(const int  &levelIndex) {
        LOG(INFO) << "[clx] <MarkerStackNode::setlevelIndex> setlevelIndex: " << levelIndex;
        ArkUI_NumberValue indexValue[] = {{.i32 = levelIndex}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        LOG(INFO) << "[clx] <MarkerStackNode::setlevelIndex> setlevelIndex: " << levelIndex;
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BACKGROUND_COLOR, &indexItem));
        return *this;
    }
} // namespace rnoh