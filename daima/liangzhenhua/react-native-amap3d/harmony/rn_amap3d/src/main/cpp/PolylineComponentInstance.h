#ifndef HARMONY_POLYLINECOMPONENTINSTANCE_H
#define HARMONY_POLYLINECOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "PolylineStackNode.h"
#include "ShadowNodes.h"

namespace rnoh {
    class PolylineComponentInstance : public CppComponentInstance<facebook::react::PolylineShadowNode> {
    private:
        PolylineStackNode m_stackNode;
        // 节点坐标
        std::multimap<float, float> points;
        // 线段宽度
        float width = 1.f;
        // 线段颜色
        int color;
        // 多段颜色
        std::vector<int> colors;
        // 层级
        float levelIndex;
        // 是否绘制大地线
        bool geodesic;
        // 是否绘制虚线
        bool dotted;
        //是否颜色渐变
        bool gradient;

    public:
        PolylineComponentInstance(Context context);
        void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
        void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
        PolylineStackNode &getLocalRootArkUINode() override;
        void onPropsChanged(SharedConcreteProps const &props) override;
        void getNapiProps(facebook::react::Props::Shared props);
    };
} // namespace rnoh

#endif 