#include "PolygonStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    PolygonStackNode::PolygonStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void PolygonStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->addChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    void PolygonStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }
    PolygonStackNode &PolygonStackNode::setPoints(const  std::multimap<float,float>  &points) {
        for (auto it = points.begin(); it != points.end(); it = points.upper_bound(it->first)) {
            ArkUI_NumberValue indexValue[] = {{.f32 = it->first}, {.f32 = it->second}};
            ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
            maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_POSITION, &indexItem));
            LOG(INFO) << "[clx] <PolygonStackNode::setPoints> it->first:" << indexItem.value[0].f32;
            LOG(INFO) << "[clx] <PolygonStackNode::setPoints> it->second:" << indexItem.value[1].f32;
        }
        return *this;
    }
    PolygonStackNode &PolygonStackNode::setStrokeWidth(const float &strokeWidth) {
        ArkUI_NumberValue indexValue[] = {{.f32 = strokeWidth}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_WIDTH, &indexItem));
        LOG(INFO) << "[clx] <PolygonStackNode::setStrokeWidth> elevation:" << indexItem.value[0].f32;
        return *this;
    }
	    PolygonStackNode &PolygonStackNode::setStrokeColor(const int &strokeColor) {
        ArkUI_NumberValue indexValue[] = {{.i32 = strokeColor}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolygonStackNode::setStrokeColor> elevation:" << strokeColor;
        return *this;
    }
    PolygonStackNode &PolygonStackNode::setFillColor(const int &fillColor) {
        ArkUI_NumberValue indexValue[] = {{.i32 = fillColor}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BACKGROUND_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolygonStackNode::setfillColor> :" << fillColor;
        return *this;
    }
    PolygonStackNode &PolygonStackNode::setLevelIndex(const float  &levelIndex) {
        ArkUI_NumberValue indexValue[] = {{.f32 = levelIndex}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(levelIndex) / sizeof(ArkUI_NumberValue)};
        //maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BACKGROUND_COLOR, &indexItem));
        LOG(INFO) << "[clx] <PolygonStackNode::levelIndex> levelIndex:" << levelIndex;
        return *this;
    }
} // namespace rnoh