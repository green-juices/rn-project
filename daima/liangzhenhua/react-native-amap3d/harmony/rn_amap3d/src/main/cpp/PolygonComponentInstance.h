#ifndef HARMONY_POLYGONCOMPONENTINSTANCE_H
#define HARMONY_POLYGONCOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "PolygonStackNode.h"
#include "ShadowNodes.h"

namespace rnoh {
    class PolygonComponentInstance : public CppComponentInstance<facebook::react::PolygonShadowNode> {
    private:
        PolygonStackNode m_stackNode;
        // 节点坐标
         std::multimap<float, float> points;
        // 边线宽度
        float strokeWidth;
        // 边线颜色
        std::string strokeColor;
        // 填充颜色
        std::string fillColor;
        // 层级
        int levelIndex;

    public:
        PolygonComponentInstance(Context context);
        void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
        void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
        PolygonStackNode &getLocalRootArkUINode() override;
        void onPropsChanged(SharedConcreteProps const &props) override;
        void getNapiProps(facebook::react::Props::Shared props);
    };
} // namespace rnoh

#endif 