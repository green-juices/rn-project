#include "MapViewComponentInstance.h"

#include <glog/logging.h>

namespace rnoh {
MapViewComponentInstance::MapViewComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void MapViewComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void MapViewComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

MapViewStackNode &MapViewComponentInstance::getLocalRootArkUINode() { return m_stackNode; }
void MapViewComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::MapViewProps>(props);
    if (p == nullptr) {
        return;
    }else {
        
        this->myLocationEnabled = p->myLocationEnabled;
        this->indoorViewEnabled = p->indoorViewEnabled;
        this->buildingsEnabled = p->buildingsEnabled;
        this->compassEnabled = p->compassEnabled;
        this->zoomControlsEnabled = p->zoomControlsEnabled;
        this->scaleControlsEnabled = p->scaleControlsEnabled;
        this->language = p->language;
        this->myLocationButtonEnabled = p->myLocationButtonEnabled;
        this->trafficEnabled = p->trafficEnabled;
        this->maxZoom = p->maxZoom;
        this->minZoom = p->minZoom;
        this->mapType = p->mapType;
        this->zoomGesturesEnabled = p->zoomGesturesEnabled;
        this->rotateGesturesEnabled = p->rotateGesturesEnabled;
        this->scrollGesturesEnabled = p->scrollGesturesEnabled;
        this->tiltGesturesEnabled = p->tiltGesturesEnabled;

        this->getLocalRootArkUINode().setMyLocationEnabled(p->myLocationEnabled);
        this->getLocalRootArkUINode().setIndoorViewEnabled(p->indoorViewEnabled);
        this->getLocalRootArkUINode().setBuildingsEnabled(p->buildingsEnabled);
        this->getLocalRootArkUINode().setCompassEnabled(p->compassEnabled);
        this->getLocalRootArkUINode().setZoomControlsEnabled(p->zoomControlsEnabled);
        this->getLocalRootArkUINode().setScaleControlsEnabled(p->scaleControlsEnabled);
        this->getLocalRootArkUINode().setLanguage(p->language);
        this->getLocalRootArkUINode().setMyLocationButtonEnabled(p->myLocationButtonEnabled);
        this->getLocalRootArkUINode().setTrafficEnabled(p->trafficEnabled);
        this->getLocalRootArkUINode().setMaxZoom(p->maxZoom);
        this->getLocalRootArkUINode().setMinZoom(p->minZoom);
        this->getLocalRootArkUINode().setMapType(p->mapType);
        this->getLocalRootArkUINode().setZoomGesturesEnabled(p->zoomGesturesEnabled);
        this->getLocalRootArkUINode().setRotateGesturesEnabled(p->rotateGesturesEnabled);
        this->getLocalRootArkUINode().setScrollGesturesEnabled(p->scrollGesturesEnabled);
        this->getLocalRootArkUINode().setTiltGesturesEnabled(p->tiltGesturesEnabled);
    }
}
void MapViewComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::MapViewProps>(props);

    this->myLocationEnabled = p->myLocationEnabled;
    this->indoorViewEnabled = p->indoorViewEnabled;
    this->buildingsEnabled = p->buildingsEnabled;
    this->compassEnabled = p->compassEnabled;
    this->zoomControlsEnabled = p->zoomControlsEnabled;
    this->scaleControlsEnabled = p->scaleControlsEnabled;
    this->language = p->language;
    this->myLocationButtonEnabled = p->myLocationButtonEnabled;
    this->trafficEnabled = p->trafficEnabled;
    this->maxZoom = p->maxZoom;
    this->minZoom = p->minZoom;
    this->mapType = p->mapType;
    this->zoomGesturesEnabled = p->zoomGesturesEnabled;
    this->rotateGesturesEnabled = p->rotateGesturesEnabled;
    this->scrollGesturesEnabled = p->scrollGesturesEnabled;
    this->tiltGesturesEnabled = p->tiltGesturesEnabled;
}
} // namespace rnoh