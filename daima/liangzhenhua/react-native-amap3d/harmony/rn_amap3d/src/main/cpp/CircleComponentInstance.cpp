/**
 * MIT License
 *
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 #include "CircleComponentInstance.h"
#include <glog/logging.h>

namespace rnoh {
CircleComponentInstance::CircleComponentInstance(Context context) : CppComponentInstance(std::move(context)) {}

void CircleComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                               std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    m_stackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
}
void CircleComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    m_stackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

CircleStackNode &CircleComponentInstance::getLocalRootArkUINode() { return m_stackNode; }

void CircleComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    CppComponentInstance::onPropsChanged(props);
    auto p = std::dynamic_pointer_cast<const facebook::react::CircleProps>(props);
    if (p == nullptr) {
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> strokeWidth: ";
        return;
    } else {
        LOG(INFO) << "[clx] <CardViewComponentInstance::onPropsChanged> Failed to cast props to RNCardViewProps.";
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> radius: " << props->radius;
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> setstrokeColor: " << props->strokeColor;
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> fillColor: " << props->fillColor;
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> strokeWidth: " << props->strokeWidth;
        LOG(INFO) << "[clx] <CircleComponentInstance::setProps> levelIndex: " << props->levelIndex;
        this->radius = p->radius;
        this->strokeWidth = p->strokeWidth;
        this->strokeColor = p->strokeColor;
        this->fillColor = p->fillColor;
        this->levelIndex = p->levelIndex;
        this->getLocalRootArkUINode().setRadius(p->radius);
        this->getLocalRootArkUINode().setStrokeWidth(p->strokeWidth);
        this->getLocalRootArkUINode().setStrokeColor(p->strokeColor);
        this->getLocalRootArkUINode().setFillColor(p->fillColor);
        this->getLocalRootArkUINode().setLevelIndex(p->levelIndex);
    }
}
void CircleComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
    auto p = std::dynamic_pointer_cast<const facebook::react::CircleProps>(props);
    this->radius = p->radius;
    this->strokeWidth = p->strokeWidth;
    this->strokeColor = p->strokeColor;
    this->fillColor = p->fillColor;
    this->levelIndex = p->levelIndex;
}
}