#include "MapViewStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    MapViewStackNode::MapViewStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void MapViewStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->insertChildAt(m_nodeHandle, child.getArkUINodeHandle(), index));
    }

    void MapViewStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }
    
    MapViewStackNode &MapViewStackNode::setMyLocationEnabled(const bool &myLocationEnabled){
        if (!myLocationEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(myLocationEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setMyLocationEnabled:" << myLocationEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setIndoorViewEnabled(const bool &indoorViewEnabled){
        if (!indoorViewEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(indoorViewEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setIndoorViewEnabled:" << indoorViewEnabled;
        return *this;
    }
	MapViewStackNode &MapViewStackNode::setBuildingsEnabled(const bool &buildingsEnabled){
        if (!buildingsEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(buildingsEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setBuildingsEnabled:" << buildingsEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setCompassEnabled(const bool &compassEnabled){
        if (!compassEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(compassEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setCompassEnabled:" << compassEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setZoomControlsEnabled(const bool &zoomControlsEnabled){
        if (!zoomControlsEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(zoomControlsEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setZoomControlsEnabled:" << zoomControlsEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setScaleControlsEnabled(const bool &scaleControlsEnabled){
        if (!scaleControlsEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(scaleControlsEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setScaleControlsEnabled:" << scaleControlsEnabled;
        return *this;
    }
	MapViewStackNode &MapViewStackNode::setLanguage(const std::string &language){
        ArkUI_AttributeItem indexItem = {.string = language.c_str()};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setLanguage:" << language;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setMyLocationButtonEnabled(const bool &myLocationButtonEnabled){
        if (!myLocationButtonEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(myLocationButtonEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setMyLocationButtonEnabled:" << myLocationButtonEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setTrafficEnabled(const bool &trafficEnabled){
        if (!trafficEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(trafficEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setTrafficEnabled:" << trafficEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setMaxZoom(const float &maxZoom){
        ArkUI_NumberValue indexValue[] = {{.f32 = maxZoom}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_WIDTH, &indexItem));
        LOG(INFO) << "[clx] <MapViewStackNode::setMapType> elevation:" << maxZoom;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setMinZoom(const float &minZoom) {
        ArkUI_NumberValue indexValue[] = {{.f32 = minZoom}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_WIDTH, &indexItem));
        LOG(INFO) << "[clx] <MapViewStackNode::setMapType> elevation:" << minZoom;
        return *this;
    }
	MapViewStackNode &MapViewStackNode::setMapType(const int &mapType) {
        ArkUI_NumberValue indexValue[] = {{.i32 = mapType}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_WIDTH, &indexItem));
        LOG(INFO) << "[clx] <MapViewStackNode::setMapType> elevation:" << mapType;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setZoomGesturesEnabled(const bool &zoomGesturesEnabled){
        if (!zoomGesturesEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(zoomGesturesEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setZoomGesturesEnabled:" << zoomGesturesEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setScrollGesturesEnabled(const bool &scrollGesturesEnabled){
        if (!scrollGesturesEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(scrollGesturesEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setScrollGesturesEnabled:" << scrollGesturesEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setRotateGesturesEnabled(const bool &rotateGesturesEnabled){
        if (!rotateGesturesEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(rotateGesturesEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setRotateGesturesEnabled:" << rotateGesturesEnabled;
        return *this;
    }
    MapViewStackNode &MapViewStackNode::setTiltGesturesEnabled(const bool &tiltGesturesEnabled){
        if (!tiltGesturesEnabled) {
            return *this;
        }
        ArkUI_NumberValue indexValue[] = {{static_cast<float>(tiltGesturesEnabled)}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <MarkerStackNode::setstrokeColor> setTiltGesturesEnabled:" << tiltGesturesEnabled;
        return *this;
    }

} // namespace rnoh