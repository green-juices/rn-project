#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

    class PolylineStackNode : public ArkUINode {
    public:
        PolylineStackNode();
        void insertChild(ArkUINode &child, std::size_t index);
        void removeChild(ArkUINode &child);
        PolylineStackNode &setWidth(const float &);
        PolylineStackNode &setGradient(const bool &);
        PolylineStackNode &setGeodesic(const bool &);
        PolylineStackNode &setDotted(const bool &);
        PolylineStackNode &setColor(const int &);
        PolylineStackNode &setColors(const std::vector<int> &);
        PolylineStackNode &setLevelIndex(const float &);
        PolylineStackNode &setPoints(const std::multimap<float, float> &);
    };

} // namespace rnoh