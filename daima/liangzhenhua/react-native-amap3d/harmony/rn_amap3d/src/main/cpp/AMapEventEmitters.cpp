/**
 * MIT License
 *
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "AMapEventEmitters.h"
namespace facebook {
namespace react {
void MapViewEventEmitter::onPressEvent(struct onPressEvent $event) const {
    dispatchEvent("PressEvent", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto $payload = jsi::Object(runtime);
        $payload.setProperty(runtime, "latitude", $event.latitude);
        $payload.setProperty(runtime, "longitude", $event.longitude);
        return $payload;
    });
}
void MapViewEventEmitter::onPressPoiEvent(struct MapPoiEvent $event) const {
    dispatchEvent("PressPoiEvent", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto $payload = jsi::Object(runtime);
        $payload.setProperty(runtime, "id", $event.id);
        $payload.setProperty(runtime, "name", $event.name);
        auto latlng = jsi::Object(runtime);
        latlng.setProperty(runtime, "latitude", $event.latlng.latitude);
        latlng.setProperty(runtime, "longitude", $event.latlng.longitude);
        $payload.setProperty(runtime, "latlng", $event.latlng);
        return $payload;
    });
}
void MapViewEventEmitter::onLongPressEvent(struct onLongPressEvent $event) const {
    dispatchEvent("LongPressEvent", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto $payload = jsi::Object(runtime);
        $payload.setProperty(runtime, "latitude", $event.latitude);
        $payload.setProperty(runtime, "longitude", $event.longitude);
        return $payload;
    });
}

void MapViewEventEmitter::onCameraMoveEvent(struct onCameraMoveEvent $event) const {
    dispatchEvent("CameraMoveEvent", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto cameraPosition = jsi::Object(runtime);
        cameraPosition.setProperty(runtime, "zoom", $event.cameraPosition.zoom);
        cameraPosition.setProperty(runtime, "bearing", $event.cameraPosition.bearing);
        cameraPosition.setProperty(runtime, "tilt", $event.cameraPosition.tilt);
        auto target = jsi::Object(runtime);
        target.setProperty(runtime, "longitude", $event.cameraPosition.target.longitude);
        target.setProperty(runtime, "latitude", $event.cameraPosition.target.latitude);
        cameraPosition.setProperty(runtime, "tilt", $event.cameraPosition.target);

        auto northeast = jsi::Object(runtime);
        northeast.setProperty(runtime, "longitude", $event.latLngBounds.northeast.longitude);
        northeast.setProperty(runtime, "latitude", $event.latLngBounds.northeast.latitude);
        auto southwest = jsi::Object(runtime);
        southwest.setProperty(runtime, "longitude", $event.latLngBounds.southwest.longitude);
        southwest.setProperty(runtime, "latitude", $event.latLngBounds.southwest.latitude);
        auto latLngBounds = jsi::Object(runtime);
        latLngBounds.setProperty(runtime, "northeast", $event.latLngBounds.northeast);
        latLngBounds.setProperty(runtime, "southwest", $event.latLngBounds.southwest);

        auto $payload = jsi::Object(runtime);
        $payload.setProperty(runtime, "cameraPosition", $event.cameraPosition);
        $payload.setProperty(runtime, "latLngBounds", $event.latLngBounds);
        return $payload;
    });
}

void MapViewEventEmitter::onCameraIdleEvent(struct CameraIdleEvent $event) const {
    dispatchEvent("CameraIdleEvent", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto cameraPosition = jsi::Object(runtime);
        cameraPosition.setProperty(runtime, "zoom", $event.cameraPosition.zoom);
        cameraPosition.setProperty(runtime, "bearing", $event.cameraPosition.bearing);
        cameraPosition.setProperty(runtime, "tilt", $event.cameraPosition.tilt);
        auto target = jsi::Object(runtime);
        target.setProperty(runtime, "longitude", $event.cameraPosition.target.longitude);
        target.setProperty(runtime, "latitude", $event.cameraPosition.target.latitude);
        cameraPosition.setProperty(runtime, "tilt", $event.cameraPosition.target);

        auto northeast = jsi::Object(runtime);
        northeast.setProperty(runtime, "longitude", $event.latLngBounds.northeast.longitude);
        northeast.setProperty(runtime, "latitude", $event.latLngBounds.northeast.latitude);
        auto southwest = jsi::Object(runtime);
        southwest.setProperty(runtime, "longitude", $event.latLngBounds.southwest.longitude);
        southwest.setProperty(runtime, "latitude", $event.latLngBounds.southwest.latitude);
        auto latLngBounds = jsi::Object(runtime);
        latLngBounds.setProperty(runtime, "northeast", $event.latLngBounds.northeast);
        latLngBounds.setProperty(runtime, "southwest", $event.latLngBounds.southwest);

        auto $payload = jsi::Object(runtime);
        $payload.setProperty(runtime, "cameraPosition", $event.cameraPosition);
        $payload.setProperty(runtime, "latLngBounds", $event.latLngBounds);
        return $payload;
    });
}

void MapViewEventEmitter::onLoad(struct onLoadEvent $event) const {
    dispatchEvent("Load", [$event = std::move($event)](jsi::Runtime &runtime) {
        auto $payload = jsi::Object(runtime);
        return $payload;
    });
}
} // namespace react
} // namespace facebook