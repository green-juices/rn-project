#ifndef HARMONY_CIRCLECOMPONENTINSTANCE_H
#define HARMONY_CIRCLECOMPONENTINSTANCE_H

#include "RNOH/CppComponentInstance.h"
#include "Props.h"
#include "ShadowNodes.h"
#include "CircleStackNode.h"

namespace rnoh {
class CircleComponentInstance : public CppComponentInstance<facebook::react::CircleShadowNode> {
private:
    CircleStackNode m_stackNode;

    // 圆点坐标
    std::map<float, float> circle;
    // 半径
    float radius;
    // 边线宽度
    float strokeWidth;
    // 边线颜色
    int strokeColor;
    // 填充颜色
    int fillColor;
    // 层级
    int levelIndex;

public:
    CircleComponentInstance(Context context);
    void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
    void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
    CircleStackNode &getLocalRootArkUINode() override;
    void onPropsChanged(SharedConcreteProps const &props) override;
    void getNapiProps(facebook::react::Props::Shared props);
};
}
#endif