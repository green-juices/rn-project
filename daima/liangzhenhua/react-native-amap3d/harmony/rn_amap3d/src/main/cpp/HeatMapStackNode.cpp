#include "HeatMapStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    HeatMapStackNode::HeatMapStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void HeatMapStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->insertChildAt(m_nodeHandle, child.getArkUINodeHandle(), index));
    }

    void HeatMapStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }
    HeatMapStackNode &HeatMapStackNode::setRadius(const float &radius) {

        ArkUI_NumberValue indexValue[] = {{.f32 = radius}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        LOG(INFO) << "[clx] <CircleStackNode::setCornerRadius> radius: " << radius;
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_RADIUS, &indexItem));
        return *this;
    }
    HeatMapStackNode &HeatMapStackNode::setOpacity(const float &opacity) {
        ArkUI_NumberValue indexValue[] = {{.f32 = opacity}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_OPACITY, &indexItem));
        LOG(INFO) << "[clx] <CircleStackNode::setstrokeWidth> elevation:" << opacity;
        return *this;
    }

} // namespace rnoh