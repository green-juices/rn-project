#pragma once

#include "RNOH/arkui/ArkUINode.h"
#include <react/renderer/graphics/Color.h>

namespace rnoh {

    class MapViewStackNode : public ArkUINode {
    public:
        MapViewStackNode();
        void insertChild(ArkUINode &child, std::size_t index);
        void removeChild(ArkUINode &child);
    
        MapViewStackNode &setMyLocationEnabled(const bool &);
        MapViewStackNode &setIndoorViewEnabled(const bool &);
        MapViewStackNode &setBuildingsEnabled(const bool &);
        MapViewStackNode &setCompassEnabled(const bool &);
        MapViewStackNode &setZoomControlsEnabled(const bool &);
        MapViewStackNode &setScaleControlsEnabled(const bool &);
        MapViewStackNode &setLanguage(const std::string &);
        MapViewStackNode &setMyLocationButtonEnabled(const bool &);
        MapViewStackNode &setTrafficEnabled(const bool &);
        MapViewStackNode &setMaxZoom(const float  &);
        MapViewStackNode &setMinZoom(const float  &);
        MapViewStackNode &setMapType(const int  &);
        MapViewStackNode &setZoomGesturesEnabled(const bool &);
        MapViewStackNode &setScrollGesturesEnabled(const bool &);
        MapViewStackNode &setRotateGesturesEnabled(const bool &);
        MapViewStackNode &setTiltGesturesEnabled(const bool &);
    };

} // namespace rnoh