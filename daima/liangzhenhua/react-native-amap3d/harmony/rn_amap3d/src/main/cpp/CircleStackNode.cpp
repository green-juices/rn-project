#include "CircleStackNode.h"
#include "NativeNodeApi.h"
#include "glog/logging.h"

namespace rnoh {
    CircleStackNode::CircleStackNode()
        : ArkUINode(NativeNodeApi::getInstance()->createNode(ArkUI_NodeType::ARKUI_NODE_STACK)) {}

    void CircleStackNode::insertChild(ArkUINode &child, std::size_t index) {
        maybeThrow(NativeNodeApi::getInstance()->addChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    void CircleStackNode::removeChild(ArkUINode &child) {
        maybeThrow(NativeNodeApi::getInstance()->removeChild(m_nodeHandle, child.getArkUINodeHandle()));
    }

    CircleStackNode &CircleStackNode::setRadius(const float &radius) {
        
        ArkUI_NumberValue indexValue[] = {{.f32 = radius}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        LOG(INFO) << "[clx] <CircleStackNode::setCornerRadius> radius: " << radius;
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_RADIUS, &indexItem));
        return *this;
    }
    CircleStackNode &CircleStackNode::setStrokeWidth(const float &strokeWidth) {
        ArkUI_NumberValue indexValue[] = {{.f32 = strokeWidth}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_WIDTH, &indexItem));
        LOG(INFO) << "[clx] <CircleStackNode::setstrokeWidth> elevation:" << indexItem.value[0].f32;
        return *this;
    }
    CircleStackNode &CircleStackNode::setStrokeColor(const int &strokeColor) {
        LOG(INFO) << "[clx] <CircleStackNode::setstrokeColor> elevation:" << strokeColor << "setcolor";
//         ArkUI_AttributeItem indexItem = {.string = setstrokeColor.c_str()};
        ArkUI_NumberValue indexValue[] = {{.i32 = strokeColor}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BORDER_COLOR, &indexItem));
        LOG(INFO) << "[clx] <CircleStackNode::setstrokeColor> elevation:" << strokeColor;
        return *this;
    }
    CircleStackNode &CircleStackNode::setFillColor(const int &fillColor) {
//         ArkUI_AttributeItem indexItem = {.string = setfillColor.c_str()};
        ArkUI_NumberValue indexValue[] = {{.i32 = fillColor}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BACKGROUND_COLOR, &indexItem));
        LOG(INFO) << "[clx] <CircleStackNode::setfillColor> MaxElevation:" << fillColor;
        return *this;
    }
    CircleStackNode &CircleStackNode::setLevelIndex(const int  &levelIndex) {
        //char * str = OH_NativeBundle_GetAppId();
        //LOG(INFO) << "[clx] <OH_NativeBundle_GetAppId " << str;
        LOG(INFO) << "[clx] <CircleStackNode::setlevelIndex";
        ArkUI_NumberValue indexValue[] = {{.i32 = levelIndex}};
        ArkUI_AttributeItem indexItem = {indexValue, sizeof(indexValue) / sizeof(ArkUI_NumberValue)};
        LOG(INFO) << "[clx] <CircleStackNode::setlevelIndex> setlevelIndex: " << levelIndex;
        // maybeThrow(NativeNodeApi::getInstance()->setAttribute(m_nodeHandle, NODE_BACKGROUND_COLOR, &indexItem));
        return *this;
    }
} // namespace rnoh