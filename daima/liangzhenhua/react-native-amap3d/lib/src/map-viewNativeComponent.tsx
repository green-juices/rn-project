import * as React from "react";
import {
    HostComponent,
    ViewProps,
} from "react-native";

import codegenNativeComponent from "react-native/Libraries/Utilities/codegenNativeComponent"
import { Float , DirectEventHandler} from "react-native/Libraries/Types/CodegenTypes";


enum  MapType {
    /**
   * 标准地图
   */
  Standard,

  /**
   * 卫星地图
   */
  Satellite,

  /**
   * 夜间地图
   */
  Night,

  /**
   * 导航地图
   */
  Navi,

  /**
   * 公交地图
   */
  Bus,
}

type LatLng = Readonly<{
    latitude: Float;
    longitude: Float;
}>

type LatLngBounds = Readonly<{
  /**
   * 西南坐标
   */
  southwest: {
    latitude: Float;
    longitude: Float;
  }

  /**
   * 东北坐标
   */
  northeast: {
    latitude: Float;
    longitude: Float;
  }
}>


/**
 * 地图标注点
 */
type MapPoi = Readonly<{  
    /**
     * 标注点 ID
     */
    id: string;
  
    /**
     * 标注点名称
     */
    name: string;
  
    /**
     * 标注点坐标
     */
    position: {
        latitude: Float;
        longitude: Float;
    }
}>


type CameraPosition = Readonly<{
    /**
     * 中心坐标
     */
    target?: {
        latitude: Float;
        longitude: Float;
    }

    /**
     * 缩放级别
     */
    zoom?: Float;

    /**
     * 朝向、旋转角度
     */
    bearing?: Float;

    /**
     * 倾斜角度
     */
    tilt?: Float;
}>


type onCameraMoveEvent = Readonly<{
    cameraPosition: {
        /**
         * 中心坐标
         */
        target?: {
            latitude: Float;
            longitude: Float;
        }
    
        /**
         * 缩放级别
         */
        zoom?: Float;
    
        /**
         * 朝向、旋转角度
         */
        bearing?: Float;
    
        /**
         * 倾斜角度
         */
        tilt?: Float;
      }
      latLngBounds: {
        /**
         * 西南坐标
         */
        southwest: {
            latitude: Float;
            longitude: Float;
        }
        
        /**
         * 东北坐标
         */
        northeast: {
            latitude: Float;
            longitude: Float;
        }
    }
}>

type onCameraIdleEvent = Readonly<{
    cameraPosition: {
        /**
         * 中心坐标
         */
        target?: {
            latitude: Float;
            longitude: Float;
        }
    
        /**
         * 缩放级别
         */
        zoom?: Float;
    
        /**
         * 朝向、旋转角度
         */
        bearing?: Float;
    
        /**
         * 倾斜角度
         */
        tilt?: Float;
      }
      latLngBounds: {
        /**
         * 西南坐标
         */
        southwest: {
            latitude: Float;
            longitude: Float;
        }
        
        /**
         * 东北坐标
         */
        northeast: {
            latitude: Float;
            longitude: Float;
        }
    }
}>

type voidEvent = Readonly<{}>

export interface MapViewProps extends ViewProps {
  /**
   * 地图类型
   */
  mapType?: MapType;

  /**
   * 初始状态
   */
  initialCameraPosition?: CameraPosition;

  /**
   * 是否显示当前定位
   */
  myLocationEnabled?: boolean;

  /**
   * 是否显示室内地图
   */
  indoorViewEnabled?: boolean;

  /**
   * 是否显示3D建筑
   */
  buildingsEnabled?: boolean;

  /**
   * 是否显示标注
   */
  labelsEnabled?: boolean;

  /**
   * 是否显示指南针
   */
  compassEnabled?: boolean;

  /**
   * 是否显示放大缩小按钮
   *
   * @platform android
   */
  zoomControlsEnabled?: boolean;

  /**
   * 是否显示比例尺
   */
  scaleControlsEnabled?: boolean;

  /**
   * 是否显示定位按钮
   *
   * @platform android
   */
  myLocationButtonEnabled?: boolean;

  /**
   * 是否显示路况
   */
  trafficEnabled?: boolean;

  /**
   * 最大缩放级别
   */
  maxZoom?: Float;

  /**
   * 最小缩放级别
   */
  minZoom?: Float;

  /**
   * 是否启用缩放手势，用于放大缩小
   */
  zoomGesturesEnabled?: boolean;

  /**
   * 是否启用滑动手势，用于平移
   */
  scrollGesturesEnabled?: boolean;

  /**
   * 是否启用旋转手势，用于调整方向
   */
  rotateGesturesEnabled?: boolean;

  /**
   * 是否启用倾斜手势，用于改变视角
   */
  tiltGesturesEnabled?: boolean;

  /**
   * 设定定位的最小更新距离
   *
   * @platform ios
   */
  distanceFilter?: Float;

  /**
   * 设定最小更新角度，默认为 1 度
   *
   * @platform ios
   */
  headingFilter?: Float;

  /**
   * 点击事件
   */
  onPress?: (DirectEventHandler<LatLng>);

  /**
   * 标注点击事件
   */
  onPressPoi?: (DirectEventHandler<MapPoi>) ;

  /**
   * 长按事件
   */
  onLongPress?: (DirectEventHandler<LatLng>) ;

  /**
   * 地图状态改变事件，随地图状态变化不停地触发
   */
  onCameraMove?: (DirectEventHandler<onCameraMoveEvent>);

  /**
   * 地图状态改变事件，在停止变化后触发
   */
  onCameraIdle?: (DirectEventHandler<onCameraIdleEvent>);

  /**
   * 地图初始化完成事件
   */
  onLoad?: (DirectEventHandler<voidEvent>);

  /**
   * 地图定位更新事件
   */
  //待修改
  //onLocation?: (event: NativeSyntheticEvent<GeolocationPosition>) => void;
}

export default codegenNativeComponent<MapViewProps>("AMapView") as HostComponent<MapViewProps>;
    