
import AMapView ,{MapViewProps} from './map-viewNativeComponent'
import React , {Component}  from 'react';

export default class extends Component<MapViewProps> {
  static defaultProps = {
    style: { flex: 1 },
    compassEnabled: true,
    scaleControlsEnabled: true,
    distanceFilter: 1,
  };

  name = name;
  ref?: (React.Component<MapViewProps> & NativeMethods) | null;
  state = { loaded: false };
  callbackMap: { [key: number]: (data: any) => void } = {};

  /**
   * 移动视角
   */
  moveCamera(cameraPosition: CameraPosition, duration = 0) {
    this.invoke("moveCamera", [cameraPosition, duration]);
  }

  /**
   * 点坐标转地理坐标，主要用于地图选点
   */
  getLatLng(point: Point): Promise<LatLng> {
    return this.call("getLatLng", point);
  }

  callback = ({ nativeEvent }: NativeSyntheticEvent<{ id: number; data: any }>) => {
    this.callbackMap[nativeEvent.id]?.call(this, nativeEvent.data);
    delete this.callbackMap[nativeEvent.id];
  };

  call(name: string, args: any): Promise<any> {
    const id = Math.random();
    this.invoke("call", [id, name, args]);
    return new Promise((resolve) => (this.callbackMap[id] = resolve));
  }

  componentDidMount() {
    super.componentDidMount();
    // 无论如何也要在 1 秒后 setLoaded(true) ，防止 onLoad 事件不触发的情况下显示不正常
    // 目前只在 iOS 上低概率出现
    setTimeout(() => this.setState({ loaded: true }), 1000);
  }

  render() {
    let { style, onLoad } = this.props;
    if (!this.state.loaded) {
      style = [style, { width: 1, height: 1 }];
    }
    return (
      <AMapView
        {...this.props}
        ref={(ref) => (this.ref = ref)}
        style={style}
        // @ts-ignore: 内部接口
        onCallback={this.callback}
        onPress={(event) => {
          if (event.nativeEvent.latitude) {
            this.props.onPress?.call(this, event);
          }
        }}
        onLoad={(event) => {
          // android 地图部分控件不显示的问题在重新 layout 之后会恢复正常。
          // 同时也能修复 ios 地图偶尔出现的 layout 异常
          this.setState({ loaded: true });
          onLoad?.call(this, event);
        }}
      />
    );
  }
}
