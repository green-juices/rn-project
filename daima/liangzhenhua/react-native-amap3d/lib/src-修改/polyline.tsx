
import AMapPolyline ,{PolylineProps} from './polylineNativeComponent'
import React , {Component}  from 'react';


export default class Polyline extends Component<PolylineProps> {
 
  constructor(props: PolylineProps) {
    super(props);
  }

  render() {
    return (
      <AMapPolyline {...this.props} />
    );
  }
}
