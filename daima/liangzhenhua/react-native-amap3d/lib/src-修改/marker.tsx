
import AMapMarker ,{MarkerProps} from './markerNativeComponent'
import React , {Component}  from 'react';


export default class Marker extends Component<MarkerProps> {
 
  constructor(props: MarkerProps) {
    super(props);
  }

  render() {
    return (
      <AMapMarker {...this.props} />
    );
  }
}
