import React from 'react';
import { View, Button } from 'react-native';
import SafeModule from 'react-native-safe-module';

const App = () => {
  const handlePress = () => {
    const testModule = SafeModule.create({
        moduleName: "ToastModule",
        mock: {
            show: () => {},
        }
    });
    testModule.show('Hello World!');
  };
  
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Button onPress={handlePress} title="Press Me" />
    </View>
  );
};

export default App;