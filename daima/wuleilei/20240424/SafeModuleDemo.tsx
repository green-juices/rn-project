import React from 'react';
import { View, Button } from 'react-native';
import SafeModule from '@react-native-oh-tpl/react-native-safe-module';

const App = () => {
  const handlePress = () => {
    console.log("SafeModuleLog1");
    const testModule = SafeModule.create({
        moduleName: "ToastModule",
        mock: {
            show: () => {
                console.log("SafeModuleLog2");
                testModule.show('Hello World!2');
                console.log("SafeModuleLog11");
            },
        }
    });
    console.log("SafeModuleLog3");
    testModule.show('Hello World!1');
    console.log("SafeModuleLog10");
  };
  
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Button onPress={handlePress} title="Press Me" />
    </View>
  );
};

export default App;