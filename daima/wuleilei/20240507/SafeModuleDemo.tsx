import React from 'react';
import { View, Button } from 'react-native';
import SafeModule from '@react-native-oh-tpl/react-native-safe-module';

const App = () => {
  const handlePress = () => {
    console.log("SafeModuleLog1");
    const testModule = SafeModule.create({
        moduleName: "ToastModule",
        mock: {
            show: () => Promise.resolve(),
        }
    });
    console.log("SafeModuleLog3");
    testModule.show('Hello World!1');
    console.log("SafeModuleLog10");
  };

  const NativeLottieView = SafeModule.component({
    viewName: 'LottieAnimationView',
    mockComponent: View,
  });
//   <Button onPress={handlePress} title="Press Me" />
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      
      <NativeLottieView
        style={{backgroundColor: 'red', width: 100, height: 100}}
        source={undefined}
        sourceName={undefined}
        sourceJson={undefined}
      />
    </View>
  );
};

export default App;