
#include "AMapGeolocationTurboModule.h"

using namespace rnoh;
using namespace facebook;

static jsi::Value hostFunction_AMapGeolocationTurboModule_init(jsi::Runtime &rt, react::TurboModule &turboModule,
                                                              const jsi::Value *args, size_t count) {
    return static_cast<ArkTSTurboModule &>(turboModule).callAsync(rt, "init", args, count);
}

AMapGeolocationTurboModule::AMapGeolocationTurboModule(const ArkTSTurboModule::Context ctx, const std::string name)
    : ArkTSTurboModule(ctx, name) {
    methodMap_["init"] = MethodMetadata{1, hostFunction_AMapGeolocationTurboModule_init};
}