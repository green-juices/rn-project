/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef HARMONY_RECTF_H
#define HARMONY_RECTF_H
namespace rnoh{
    class RectF {
    public:
        float left;
        float top;
        float right;
        float bottom;
        float width();
        float height();
        void set(float left, float top, float right, float bottom);
        RectF();
        RectF(float left, float top, float right, float bottom);
    };
};
#endif //HARMONY_RECTF_H
