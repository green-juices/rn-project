import type { TurboModule } from "react-native";
import { TurboModuleRegistry } from "react-native";

export interface Spec extends TurboModule {
  init(key): void;
}

export default TurboModuleRegistry.get<Spec>("AMapGeolocation")as Spec;