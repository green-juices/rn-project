import { RNPackage, TurboModulesFactory } from 'rnoh/ts';
import type { TurboModule, TurboModuleContext } from 'rnoh/ts';
import { AMapGeolocationLoadModule } from './AMapGeolocationModule';

class AMapGeolocationModulesFactory extends TurboModulesFactory {
  createTurboModule(name: string): TurboModule | null {
    if (name === 'AMapGeolocation') {
      return new AMapGeolocationLoadModule(this.ctx)
    }
    return null;
  }

  hasTurboModule(name: string): boolean {
    return name === 'AMapGeolocation';
  }
}

export class AMapGeolocationPackage extends RNPackage {
  createTurboModulesFactory(ctx: TurboModuleContext): TurboModulesFactory {
    return new AMapGeolocationModulesFactory(ctx);
  }
}