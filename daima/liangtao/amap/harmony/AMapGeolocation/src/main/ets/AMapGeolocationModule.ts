import { TurboModule, TurboModuleContext } from "rnoh/ts";
import hilog from '@ohos.hilog';

import {
  IAMapLocationListener,
  AMapLocationManagerImpl,
  AMapLocationOption,
  AMapLocation
} from '@amap/amap_lbs_location';

import abilityAccessCtrl, { PermissionRequestResult } from '@ohos.abilityAccessCtrl';
import common from '@ohos.app.ability.common';
import { AMapPrivacyShowStatus, AMapPrivacyInfoStatus, AMapPrivacyAgreeStatus, } from '@amap/amap_lbs_common'

import { AMapLocationReGeocodeLanguage } from '@amap/amap_lbs_location'

import { CommonConstants } from './CommonConstants'
import geoLocationManager from '@ohos.geoLocationManager';
import systemDateTime from '@ohos.systemDateTime';
import { AMapLocationType } from '@amap/amap_lbs_location/src/main/ets/com/amap/interface/AMapLocationCommonObj';
import bundleManager from '@ohos.bundle.bundleManager';
import { Permissions } from '@ohos.abilityAccessCtrl';
import { e45 } from '@amap/amap_lbs_location/src/main/ets/com/amap/location/AMapLocationErrorInfo';

import { BusinessError } from '@ohos.base';
import { Context } from '@kit.AbilityKit';

export class AMapGeolocationLoadModule extends TurboModule implements IAMapLocationListener {
  private options: AMapLocationOption | null = null;
  // private  eventEmitter:DeviceEventManagerModule.RCTDeviceEventEmitter= null;
  private client: AMapLocationManagerImpl | null = null;
  private context: TurboModuleContext | null = null;
  private reGeocodeLanguage: AMapLocationReGeocodeLanguage = AMapLocationReGeocodeLanguage.Chinese;
  private interval: number = 5;

  constructor(ctx: TurboModuleContext) {
    super(ctx);
    hilog.debug(0xcc00, '[RNOH]', ':FileUpLoadModule constructor');
    this.options = {
      priority: geoLocationManager.LocationRequestPriority.FIRST_FIX,
      scenario: geoLocationManager.LocationRequestScenario.UNSET,
      maxAccuracy: 0,
      singleLocationTimeout: 3000,
      allowsBackgroundLocationUpdates: false,
      locatingWithReGeocode: false,
      reGeocodeLanguage: AMapLocationReGeocodeLanguage.Chinese,
      isOffset: false
    }

  }

  onLocationChanged(location: AMapLocation): void {
    this.ctx.rnInstance.emitDeviceEvent(
      "AMapGeolocation",
      this.toJSON(location)
    )
  };

  onLocationError(locationErrorInfo: e45): void {

  };

  requestPermissions(): void {
    let atManager = abilityAccessCtrl.createAtManager();
    try {

      atManager.requestPermissionsFromUser(this.ctx.uiAbilityContext, CommonConstants.REQUEST_PERMISSIONS)
        .then((data) => {
          console.error('AMapGeolocation requestPermissionsFromUser')

          if (data.authResults[0] !== 0 || data.authResults[1] !== 0) {
            console.error('AMapGeolocation requestPermissionsFromUser return')
            return;
          }
          const that = this;

        })
        .catch((err: Error) => {
          console.error('AMapGeolocation requestPermissionsFromUser err' + JSON.stringify(err))
        })
    } catch (err) {
      console.error('AMapGeolocation requestPermissionsFromUser err' + JSON.stringify(err));
    }
  }

  async checkAccessToken(permission: Permissions): Promise<abilityAccessCtrl.GrantStatus> {
    let atManager: abilityAccessCtrl.AtManager = abilityAccessCtrl.createAtManager();
    let grantStatus: abilityAccessCtrl.GrantStatus = abilityAccessCtrl.GrantStatus.PERMISSION_DENIED;

    // 获取应用程序的accessTokenID
    let tokenId: number = 0;
    try {
      let bundleInfo: bundleManager.BundleInfo = await bundleManager.getBundleInfoForSelf(bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION);
      let appInfo: bundleManager.ApplicationInfo = bundleInfo.appInfo;
      tokenId = appInfo.accessTokenId;
    } catch (error) {
      let err: BusinessError = error as BusinessError;
      console.error(`AMapGeolocation Failed to get bundle info for self. Code is ${err.code}, message is ${err.message}`);
    }

    // 校验应用是否被授予权限
    try {
      grantStatus = await atManager.checkAccessToken(tokenId, permission);
    } catch (error) {
      let err: BusinessError = error as BusinessError;
      console.error(`AMapGeolocation Failed to check access token. Code is ${err.code}, message is ${err.message}`);
    }

    return grantStatus;
  }

  async checkPermissions(): Promise<void> {
    const permissions: Array<Permissions> = ['ohos.permission.LOCATION'];
    let grantStatus: abilityAccessCtrl.GrantStatus = await this.checkAccessToken(permissions[0]);

    if (grantStatus === abilityAccessCtrl.GrantStatus.PERMISSION_GRANTED) {
      // 已经授权，可以继续访问目标操作
    } else {
      // 申请日历权限
      const permissions: Array<Permissions> = ['ohos.permission.LOCATION'];

      this.reqPermissionsFromUser(CommonConstants.REQUEST_PERMISSIONS);
    }
  }

  reqPermissionsFromUser(permissions: Array<Permissions>): void {
    let context: ESObject = this.ctx.uiAbilityContext;
    let atManager: abilityAccessCtrl.AtManager = abilityAccessCtrl.createAtManager();
    // requestPermissionsFromUser会判断权限的授权状态来决定是否唤起弹窗
    atManager.requestPermissionsFromUser(context, permissions).then((data: PermissionRequestResult) => {
      let grantStatus: Array<number> = data.authResults;
      let length: number = grantStatus.length;
      for (let i = 0; i < length; i++) {
        if (grantStatus[i] === 0) {
          // 用户授权，可以继续访问目标操作
        } else {
          // 用户拒绝授权，提示用户必须授权才能访问当前页面的功能，并引导用户到系统设置中打开相应的权限
          // this.openPermissionsInSystemSettings()
          return;
        }
      }
      // 授权成功
    }).catch((err: BusinessError) => {
      console.error(`Failed to request permissions from user. Code is ${err.code}, message is ${err.message}`);
    })
  }

  public init(key: string): void {
    let context: ESObject = this.ctx.uiAbilityContext;
    this.checkPermissions();
    if (this.client != null) {
      this.client.stopContinuousTask();
    }
    hilog.debug(0xcc00, '[RNOH]', ':geolocation init start:');
    AMapLocationManagerImpl.setApiKey(key);
    AMapLocationManagerImpl.updatePrivacyShow(AMapPrivacyShowStatus.DidShow, AMapPrivacyInfoStatus.DidContain, context);
    AMapLocationManagerImpl.updatePrivacyAgree(AMapPrivacyAgreeStatus.DidAgree,context);
    if (this.client === undefined) {
      this.client = new AMapLocationManagerImpl(context);

    }
    this.client?.setLocationListener(AMapLocationType.Single, this)
  }

  public start(): void {
    this.client?.startContinuousTask();
  }

  public stop(): void {
    this.client?.stopContinuousTask()
  }


  public getLastKnownLocation(): void {
    let options: AMapLocationOption = {
      allowsBackgroundLocationUpdates: true,
      locatingWithReGeocode: true,
      reGeocodeLanguage: AMapLocationReGeocodeLanguage.Chinese,
      isOffset: false
    }
    this.client?.setLocationOption(AMapLocationType.Last, options);
    this.client?.setLocationListener(AMapLocationType.Last, this);
    this.client?.requestLastLocation();

  }

  public setOnceLocation(): void {
    this.client?.setLocationListener(AMapLocationType.Single, this)
    this.client?.setLocationOption(AMapLocationType.Single, this.options)
    this.client?.requestSingleLocation()
  }

  public startUpdatingLocation(): void {
    let options: AMapLocationOption = {
      priority: geoLocationManager.LocationRequestPriority.FIRST_FIX,
      scenario: geoLocationManager.LocationRequestScenario.UNSET,
      timeInterval: 2,
      distanceInterval: 0,
      maxAccuracy: 20,
      allowsBackgroundLocationUpdates: false,
      singleLocationTimeout: 5000,
      locatingWithReGeocode: true,
      reGeocodeLanguage: AMapLocationReGeocodeLanguage.Chinese,
      isOffset: true
    }

    this.client?.setLocationListener(AMapLocationType.Updating, this)
    this.client?.setLocationOption(AMapLocationType.Updating, options)
    this.client?.startUpdatingLocation()
  }

  public stopUpdatingLocation() {
    this.client?.stopUpdatingLocation()
  }

  public setInterval(interval: number): void {
    if (this.options != null) this.options.timeInterval = interval;
  }

  public setGeoLanguage(language: AMapLocationReGeocodeLanguage): void {
    if (this.options != null) this.options.reGeocodeLanguage = language;
  }

  public setNeedAddress(value: boolean): void {
    if (this.options != null) this.options.locatingWithReGeocode = value;
  }

  public setLocationTimeout(value: number): void {
    if (this.options != null) this.options.singleLocationTimeout = value;
  }

  public setAllowsBackgroundLocationUpdates(isAllow: boolean): void {
    if (this.options != null) this.options.allowsBackgroundLocationUpdates = isAllow;
  }

  public setDistanceFilter(distance: number): void{
    if(this.options !=null) this.options.distanceInterval = distance;
  }
  public setDesiredAccuracy(desiredAccuracy: number): void{
    if (this.options !=null) this.options.maxAccuracy = desiredAccuracy;
  }
  private toJSON(location: AMapLocation): Map<string, ESObject> {
    if (location == null) {
      return new Map<string, ESObject>();
    }
    let map: Map<string, ESObject> = new Map<string, ESObject>();
    map.set("timestamp", location.timeStamp);
    map.set("accuracy", location.accuracy);
    map.set("latitude", location.latitude);
    map.set("longitude", location.longitude);
    map.set("altitude", location.altitude);
    map.set("speed", location.speed);
    map.set("direction", location.direction);
    map.set("timeSinceBoot", location.timeSinceBoot);
    map.set("additions", location.additions);
    map.set("additionSize", location.additionSize);
    map.set("isOffset", location.isOffset);
    map.set("reGeo", location.reGeo);
    if (location.reGeo != undefined) {
      map.set("address", location?.reGeo.address);
      map.set("description", location.reGeo.desc);
      map.set("poiName", location.reGeo.poiName);
      map.set("country", location.reGeo.country);
      map.set("province", location.reGeo.province);
      map.set("city", location.reGeo.city);
      map.set("cityCode", location.reGeo.citycode);
      map.set("district", location.reGeo.district);
      map.set("street", location.reGeo.street);
      map.set("adCode", location.reGeo.adcode);
    }
    return map;
  }
}