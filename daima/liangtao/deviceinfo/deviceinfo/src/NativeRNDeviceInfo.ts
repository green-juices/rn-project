import type { TurboModule } from 'react-native';
import { TurboModuleRegistry } from 'react-native';
// import { TurboModule, TurboModuleRegistry} from 'react-native';

export interface Spec extends TurboModule {  
    // getAndroidId(): Promise<string>; // todo android
    // getAndroidIdSync(): string; // todo android
    getApiLevel(): Promise<number>; // sss
    getApiLevelSync(): number; // sss
    getApplicationName(): string; // sss
    //getAvailableLocationProviders(): Promise<Object>; //  todo aaa
    //getAvailableLocationProvidersSync(): Object; // todo aaa
    getBaseOs(): Promise<string>; // sss
    getBaseOsSync:() => string; // sss
    getBatteryLevel(): Promise<number>; // sss
    getBatteryLevelSync(): number; // sss
    getBootloader(): Promise<string>; // sss
    getBootloaderSync(): string; // sss
    getBrand(): string; // sss
    getBuildId(): Promise<string>; // sss
    getBuildIdSync(): string; // sss
    getBuildNumber(): string; // sss
    getBundleId(): string; // sss
    getCarrier(): Promise<string>; // sss
    getCarrierSync(): string; // sss
    getCodename(): Promise<string>; // sss
    getCodenameSync(): string; // sss
    getDevice(): Promise<string>; // sss
    getDeviceSync(): string; // sss
    getDeviceId(): string; // todo aaa
    getDeviceName(): Promise<string>; // sss
    getDeviceNameSync(): string; // sss
    // getDeviceToken(): Promise<string>; // todo ios
    getDeviceType(): string; // sss
    getDisplay(): Promise<string>; // sss
    getDisplaySync(): string; // sss
    getFingerprint(): Promise<string>; // sss
    getFingerprintSync(): string; // sss
    getFirstInstallTime(): Promise<number>; // sss
    getFirstInstallTimeSync(): number; // sss
    getFontScale(): Promise<number>; // sss
    getFontScaleSync(): number; // sss
    getFreeDiskStorage(): Promise<number>; // sss
    getFreeDiskStorageOld(): Promise<number>; // sss
    getFreeDiskStorageSync(): number; // sss
    getFreeDiskStorageOldSync(): number; // sss
    getHardware(): Promise<string>; // sss
    getHardwareSync(): string; // sss
    getHost(): Promise<string>; // sss
    getHostSync(): string; // sss
    // getHostNames(): Promise<string[]>; // todo for windows
    // getHostNamesSync(): string[]; // todo for windows
    getIncremental(): Promise<string>; // sss
    getIncrementalSync(): string; // sss
    getInstallerPackageName(): Promise<string>; // tod edit
    getInstallerPackageNameSync(): string; // tod edit
    // getInstallReferrer(): Promise<string>; // tod edit
    // getInstallReferrerSync(): string; // tod edit
    getInstanceId(): Promise<string>; // sss
    getInstanceIdSync(): string; // sss
    getIpAddress(): Promise<string>; // sss
    getIpAddressSync(): string; // sss
    getLastUpdateTime(): Promise<number>; // sss
    getLastUpdateTimeSync(): number; // sss
    getMacAddress(): Promise<string>; // sss 
    // getMacAddressSync(): string; // todo 没有提供同步方法
    getManufacturer(): Promise<string>; // sss
    getManufacturerSync(): string; // sss
    //getMaxMemory(): Promise<number>; // todo edit
    //getMaxMemorySync(): number; // todo edit
    getModel(): string; // sss
    //getPhoneNumber(): Promise<string>; // sss
    //getPhoneNumberSync(): string;
    getPowerState(): Promise<string>; // sss
    getPowerStateSync(): string; // todo aaa
    // getPreviewSdkInt(): Promise<number>; // todo android
    // getPreviewSdkIntSync(): number; // todo android
    getProduct(): Promise<string>; // sss
    //getProductSync(): string; // sss
    getReadableVersion(): string; // todo js 已实现
    getSecurityPatch(): Promise<string>; // sss
    getSecurityPatchSync(): string; // sss
    getSerialNumber(): Promise<string>; // sss
    getSerialNumberSync(): string; // sss
    // getSystemAvailableFeatures(): Promise<string[]>; // todo android
    // getSystemAvailableFeaturesSync(): string[]; // todo android
    getSystemName(): string; // sss
    getSystemVersion(): string; // sss
    getTags(): Promise<string>; // sss
    getTagsSync(): string; // sss
    getTotalDiskCapacity(): Promise<number>; // sss
    getTotalDiskCapacityOld(): Promise<number>; // sss
    getTotalDiskCapacitySync(): number; // sss
    getTotalDiskCapacityOldSync(): number; // sss
    //getTotalMemory(): Promise<number>; // todo edit
    //getTotalMemorySync(): number; // todo edit
    getType(): Promise<string>; // sss
    getTypeSync(): string; // sss
    getUniqueId(): Promise<string>; // sss
    getUniqueIdSync(): string; // sss
    //getUsedMemory(): Promise<number>; // todo edit
    //getUsedMemorySync(): number; // todo edit
    getUserAgent(): Promise<string>; // sss
    getUserAgentSync(): string; // sss
    getVersion(): string; // sss
    // getBrightness(): Promise<number>; // todo only for ios
    // getBrightnessSync(): number; // todo only for ios
    hasGms(): Promise<boolean>; // sss
    hasGmsSync(): boolean; // sss
    hasHms(): Promise<boolean>; // sss
    hasHmsSync(): boolean; // sss
    // hasNotch(): boolean; // todo rn js 实现
    // hasDynamicIsland(): boolean; //todo rn js 实现
    // hasSystemFeature(feature: string): Promise<boolean>; // todo android
    // hasSystemFeatureSync(feature: string): boolean; // todo android
    isAirplaneMode(): Promise<boolean>; // sss
    isAirplaneModeSync(): boolean; // sss
    isBatteryCharging(): Promise<boolean>; // sss
    isBatteryChargingSync(): boolean; // sss
    isCameraPresent(): Promise<boolean>; // sss
    isCameraPresentSync(): boolean; // sss
    isEmulator(): Promise<boolean>; // todo edit
    isEmulatorSync(): boolean; // todo edit
    isHeadphonesConnected(): Promise<boolean>; // sss
    isHeadphonesConnectedSync(): boolean; // todo 没有提供同步方法
    // isLandscape(): Promise<boolean>; // todo rn js方法已实现
    // isLandscapeSync(): boolean;// todo rn js方法已实现
    isWiredHeadphonesConnected(): boolean;
    isBluetoothHeadphonesConnected(): boolean;
    isLocationEnabled(): Promise<boolean>; // sss
    isLocationEnabledSync(): boolean; // sss
    //isPinOrFingerprintSet(): Promise<boolean>; // todo edit
    //isPinOrFingerprintSetSync(): boolean; // todo edit
    // isMouseConnected(): Promise<boolean>; // todo windows
    // isMouseConnectedSync(): boolean; // todo windows
    // isKeyboardConnected(): Promise<boolean>; // todo windows
    // isKeyboardConnectedSync(): boolean; // todo windows
    // isTabletMode(): Promise<boolean>; // todo windows
    isTablet(): boolean; // sss
    //isLowRamDevice(): boolean; // todo edit
    // isDisplayZoomed(): boolean; // todo IOS
    supported32BitAbis(): Promise<string[]>; // sss
    supported32BitAbisSync(): string[]; // sss
    supported64BitAbis(): Promise<string[]>; // sss
    supported64BitAbisSync(): string[]; // sss
    supportedAbis(): Promise<string[]>; // sss
    supportedAbisSync(): string[]; // sss
    // syncUniqueId(): Promise<string>;
    // useBatteryLevel(): number | null;
    // useBatteryLevelIsLow(): number | null;
    // useDeviceName(): AsyncHookResult<string>;
    // useFirstInstallTime(): AsyncHookResult<number>;
    // useHasSystemFeature(feature: string): AsyncHookResult<boolean>;
    // useIsEmulator(): AsyncHookResult<boolean>;
    // usePowerState(): Partial<PowerState>;
    // useManufacturer(): AsyncHookResult<string>;
    // useIsHeadphonesConnected(): AsyncHookResult<boolean>;
    // useBrightness(): number | null;
    getSupportedMediaTypeList(): Promise<string[]>; // sss
    getSupportedMediaTypeListSync(): string[]; // sss
}

export default TurboModuleRegistry.getEnforcing<Spec>("RNDeviceInfo");
// export default TurboModuleRegistry.get<Spec>('RNDeviceInfo') as Spec | null;