/**
 * MIT License
 *
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import deviceInfo from '@ohos.deviceInfo';
import batteryInfo from '@ohos.batteryInfo';
import bundleManager from '@ohos.bundle.bundleManager';
import resourceManager from '@ohos.resourceManager';
import camera from '@ohos.multimedia.camera';
import { BusinessError } from '@ohos.base';
import audio from '@ohos.multimedia.audio';
import deviceManager from '@ohos.distributedDeviceManager';
import sim from '@ohos.telephony.sim';
import settings from '@ohos.settings';
import statvfs from '@ohos.file.statvfs';
import geoLocationManager from '@ohos.geoLocationManager';
import { common } from '@kit.AbilityKit';
import wifiManager from '@ohos.wifiManager';
import ble from '@ohos.bluetooth.ble';
import bluetoothManager from '@ohos.bluetoothManager';
import web_webview from '@ohos.web.webview'
import power from '@ohos.power';
import media from '@ohos.multimedia.media';
import { TurboModule, TurboModuleContext } from '@rnoh/react-native-openharmony/ts';
import { TM } from '@rnoh/react-native-openharmony/generated/ts';

const abiList32 = ["armeabi", "win_x86", "win_arm"];
const abiList64 = ["arm64 v8", "Intel x86-64h Haswell", "arm64-v8a", "armeabi-v7a", "win_x64"];

export class RNDeviceInfoModule extends TurboModule implements TM.RNDeviceInfo.Spec {
    protected context: common.UIAbilityContext;

    constructor(protected ctx: TurboModuleContext) {
        super(ctx);
        this.context = ctx?.uiAbilityContext;
    }

    getApiLevelSync(): number {
        console.info('test zyx RNDeviceInfoModule getApiLevelSync')
        return deviceInfo.sdkApiVersion;
    }

    getApiLevel(): Promise<number> {
        console.info('test zyx RNDeviceInfoModule getApiLevel')
        return new Promise<number>((resolve, reject) => {
            const data = this.getApiLevelSync();
            console.info('test zyx RNDeviceInfoModule getApiLevel data:' + data)
            resolve(data);
        });
    }

    getApplicationName(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: string = "";
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            if (data) {
                result = this.context?.resourceManager?.getStringSync(data.appInfo?.labelId);
            }
        } catch (err) {
            let message = (err as BusinessError).message;
            console.error(`getBundleInfoForSelfSync failed error message: ${message}.`);
        }
        return result;
    }

    getAvailableLocationProviders(): Promise<Object> {
        return new Promise<Object>((resolve, reject) => {
            const data = this.getAvailableLocationProvidersSync();
            console.info('test zyx RNDeviceInfoModule getAvailableLocationProviders data:' + data)
            resolve(data);
        });
    }

    getAvailableLocationProvidersSync(): Object {
        let obj = {
            gps: false,
            // network: false,
            // passive: false,
        };

        try {
            let locationEnabled = geoLocationManager.isLocationEnabled();
            obj.gps = locationEnabled;
        } catch (err) {
            console.error("test zyx getAvailableLocationProvidersSync errCode:" + (err).code + ",errMessage:" +
            (err).message);
        }
        return obj;
    }

    getBaseOs(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getBaseOsSync();
            console.info('test zyx RNDeviceInfoModule getBaseOs data:' + data)
            resolve(data);
        });
    }

    getBaseOsSync(): string {
        return "HarmonyOS"
    }

    getBatteryLevel(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            resolve(this.getBatteryLevelSync());
        });
    }

    getBatteryLevelSync(): number {
        return batteryInfo.batterySOC;
    }

    getBootloader(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getBootloaderSync();
            console.info('test zyx RNDeviceInfoModule getBootloader data:' + data)
            resolve(data);
        });
    }

    getBootloaderSync(): string {
        console.info('test zyx RNDeviceInfoModule getBootloaderSync bootloaderVersion:' + deviceInfo.bootloaderVersion)
        return deviceInfo.bootloaderVersion;
    }

    getBrand(): string {
        console.info('test zyx RNDeviceInfoModule getBrand Brand:' + deviceInfo.brand)
        return deviceInfo.brand;
    }

    getBuildId(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getBuildIdSync());
        });
    }

    getBuildIdSync(): string {
        return deviceInfo.versionId;
    }

    getBuildNumber(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: string = "";
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            console.info('test zyx RNDeviceInfoModule getBuildNumber data:' + JSON.stringify(data))
            if (data) {
                result = data.versionCode.toString();
            }
        } catch (err) {
            let message = (err as BusinessError).message;
            console.error(`getBundleInfoForSelfSync aa failed error message: ${message}.`);
        }
        return result;
    }

    getBundleId(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: string = "";
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            console.info('test zyx RNDeviceInfoModule getBundleId data:' + JSON.stringify(data))
            if (data) {
                result = data.versionCode.toString();
            }
        } catch (err) {
            let message = (err as BusinessError).message;
            console.error(`getBundleInfoForSelfSync getBundleId failed error message: ${message}.`);
        }

        return result;
    }

    getCarrier(): Promise<string> {
        return sim.getSimSpn(0)
    }

    // sss 卡槽问题待处理
    getCarrierSync(): string {
        let spn: string = "";
        try {
            spn = sim.getSimSpnSync(0);
            console.info(`test zyx getCarrierSync the sim card spn is:` + spn);
        } catch (e) {
            console.info(`test zyx getCarrierSync the sim card spn err:` + e.message);
        }
        return spn;
    }

    getCodename(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getCodenameSync(); // osReleaseType osFullName
            resolve(data);
        });
    }

    getCodenameSync(): string {
        console.info('test zyx RNDeviceInfoModule getCodenameSync data:' + deviceInfo.osReleaseType)
        return deviceInfo.osReleaseType;
    }

    getDevice(): Promise<string> {
        console.info('test zyx RNDeviceInfoModule getApiLevel')
        return new Promise<string>((resolve, reject) => {
            const data = deviceInfo.hardwareModel;
            console.info('test zyx RNDeviceInfoModule getDevice data:' + data)
            resolve(deviceInfo.hardwareModel);

        });
    }

    getDeviceSync(): string {
        console.info('test zyx RNDeviceInfoModule getCodenameSync data:' + deviceInfo.hardwareModel)
        return deviceInfo.hardwareModel;
    }

    getDeviceId(): string {
        return deviceInfo.productModel;
    }

    getDeviceName(): Promise<string> {
        return settings.getValue(this.context, settings.general.DEVICE_NAME);
    }

    getDeviceNameSync(): string {
        let data = settings.getValueSync(this.context, settings.general.DEVICE_NAME, '');
        console.info("test zyx RNDeviceInfoModule getDeviceNameSync data:" + JSON.stringify(data));
        return data;
    }

    getDeviceType(): string {
        return deviceInfo.deviceType;
    }

    getDisplay(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getDisplaySync());
        });
    }

    getDisplaySync(): string {
        return deviceInfo.displayVersion;
    }

    getFingerprint(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = deviceInfo.versionId;
            resolve(this.getFingerprintSync());
        });
    }

    getFingerprintSync(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
        return data.versionCode.toString();
    }

    async getFirstInstallTime(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            const data = this.getFirstInstallTimeSync();
            resolve(data);
        });
    }

    getFirstInstallTimeSync(): number {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: number;
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            if (data) {
                result = data.installTime;
            }
        } catch (err) {
            let message = (err as BusinessError).message;
        }
        return result;
    }

    getFontScale(): Promise<number> {
        Environment.envProp('fontScale', '');
        const fontScale: SubscribedAbstractProperty<number> = AppStorage.prop('fontScale');
        return new Promise<number>((resolve, reject) => {
            const data = fontScale.get();
            console.info('test zyx RNDeviceInfoModule getFontScale data:' + data)
            resolve(fontScale.get());
        });
    }

    getFontScaleSync(): number {
        Environment.envProp('fontScale', '');
        const fontScale: SubscribedAbstractProperty<number> = AppStorage.prop('fontScale');
        return fontScale.get();
    }

    getFreeDiskStorage(): Promise<number> {
        let context = this.context;
        let path = context?.filesDir;
        return statvfs.getFreeSize(path);
    }

    getFreeDiskStorageSync(): number {
        let path = "/dev";
        let size = statvfs.getFreeSizeSync(path);
        console.info("test zyx getFreeDiskStorageSync succeed, Size: " + size);
        return size;
    }

    getFreeDiskStorageOld(): Promise<number> {
        let context = this.context;
        let path = context?.filesDir;
        return statvfs.getFreeSize(path);
    }

    getFreeDiskStorageOldSync(): number {
        let path = "/dev";
        let size = statvfs.getFreeSizeSync(path);
        console.info("test zyx getFreeDiskStorageOldSync succeed, Size: " + size);
        return size;
    }

    getHardware(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getHardwareSync());
        });
    }

    getHardwareSync(): string {
        return deviceInfo.hardwareModel;
    }

    // getValue  只在FA模式下可用
    getHost(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getHostSync());
        });
    }

    getHostSync(): string {
        return deviceInfo.buildHost;
    }

    getIncremental(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = deviceInfo.incrementalVersion;
            console.info('test zyx RNDeviceInfoModule getIncremental data:' + data)
            resolve(deviceInfo.incrementalVersion);
        });
    }

    getIncrementalSync(): string {
        console.info('test zyx RNDeviceInfoModule getIncrementalSync data:' + deviceInfo.incrementalVersion)
        return deviceInfo.incrementalVersion;
    }

    // todo edit
    getInstallerPackageName(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getInstallerPackageNameSync());
        });
    }

    // todo edit
    getInstallerPackageNameSync(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_HAP_MODULE;
        let bundleInfo = bundleManager.getBundleInfoForSelfSync(bundleFlags);
        return bundleInfo.name
    }

    getInstanceId(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getInstanceIdSync());
        });
    }

    getInstanceIdSync(): string {
        console.info('test zyx RNDeviceInfoModule getInstanceIdSync data:' + deviceInfo.udid)
        return deviceInfo.ODID;
    }

    getIpAddress(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getIpAddressSync();
            resolve(data);
        });
    }

    getIpAddressSync(): string {
        let ipAddress: string = '';
        try {
            let info = wifiManager.getIpInfo();
            ipAddress = this.intToIP(info.ipAddress)
        } catch (error) {
            console.error("failed:" + JSON.stringify(error));
        }
        return ipAddress;
    }

    private intToIP(num) {
        var str;
        var tt = new Array();
        tt[0] = (num >>> 24) >>> 0;
        tt[1] = ((num << 8) >>> 24) >>> 0;
        tt[2] = (num << 16) >>> 24;
        tt[3] = (num << 24) >>> 24;
        str = String(tt[0]) + "." + String(tt[1]) + "." + String(tt[2]) + "." + String(tt[3]);
        return str;
    }

    async getLastUpdateTime(): Promise<number> {
        let bundleFlags =
            bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION; // | bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_METADATA;
        let bundleInfo = await bundleManager.getBundleInfoForSelf(bundleFlags); //  .updateTime
        return bundleInfo.updateTime;
    }

    getLastUpdateTimeSync(): number {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: number;
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            console.info('test zyx RNDeviceInfoModule getLastUpdateTimeSync data:' + JSON.stringify(data))
            if (data) {
                result = data.updateTime;
            }
        } catch (err) {
            let message = (err as BusinessError).message;
            console.error(`test zyx getLastUpdateTimeSync failed error message: ${message}.`);
        }
        return result;
    }

    //需要系统权限
    async getMacAddress(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(wifiManager.getDeviceMacAddress().toString());
        });
    }

    getManufacturer(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getManufacturerSync());
        });
    }

    getManufacturerSync(): string {
        return deviceInfo.manufacture;
    }

    getModel(): string {
        console.info('test zyx RNDeviceInfoModule getModel data:' + deviceInfo.productModel)
        return deviceInfo.productModel;
    }

    // getPowerState(): Promise<Partial<{
    //     batteryLevel: number,
    //     batteryState: string,
    //     lowPowerMode: boolean
    // }>> {
    //     console.info('test zyx RNDeviceInfoModule getPowerState')
    //     return new Promise<Partial<{
    //         batteryLevel: number,
    //         batteryState: string, //BatteryState
    //         lowPowerMode: boolean,
    //     }>>((resolve, reject) => {
    //         let data = this.getPowerStateSync();
    //         console.info('test zyx RNDeviceInfoModule getPowerState data:' + data)
    //         resolve(data);
    //     });
    // }

    // getPowerStateSync(): {
    //     batteryLevel: number,
    //     batteryState: string,
    //     lowPowerMode: boolean
    // } {
    //     let batterySOC = batteryInfo.batterySOC;
    //     let chargingStatus = batteryInfo.chargingStatus;
    //     let batteryState: string = "unknown";
    //     if (chargingStatus == batteryInfo.BatteryChargeState.NONE ||
    //         chargingStatus == batteryInfo.BatteryChargeState.DISABLE) {
    //         batteryState = "unplugged";
    //     } else if (chargingStatus == batteryInfo.BatteryChargeState.ENABLE) { //BATTERY_STATUS_CHARGING
    //         batteryState = "charging";
    //     } else if (chargingStatus == batteryInfo.BatteryChargeState.FULL) { //BATTERY_STATUS_FULL
    //         batteryState = "full";
    //     }
    //
    //     let lowPowerMode = false;
    //     try {
    //         let mode = power.getPowerMode();
    //         if (mode == power.DevicePowerMode.MODE_POWER_SAVE || power.DevicePowerMode.MODE_EXTREME_POWER_SAVE) {
    //             lowPowerMode = true;
    //         }
    //         console.info('power mode: ' + mode);
    //     } catch (err) {
    //         console.error('get power mode failed, err: ' + err);
    //     }
    //     let data = {
    //         batteryLevel: batterySOC,
    //         batteryState: batteryState,
    //         lowPowerMode: lowPowerMode // todo sss
    //     }
    //     return data;
    // }

    getProduct(): Promise<string> {
        return settings.getValue(this.context, settings.general.DEVICE_NAME);
    }

    // async getProductSync(): Promise<string> {
    //     console.info('test zyx RNDeviceInfoModule getProductSync data:' + deviceInfo.hardwareModel)
    //     return await settings.getValue(this.context, settings.general.DEVICE_NAME);
    // }

    getReadableVersion(): string {
        return this.getVersion() + '.' + this.getBuildNumber();
    }

    getSecurityPatch(): Promise<string> {
        console.info('test zyx RNDeviceInfoModule getSecurityPatch')
        return new Promise<string>((resolve, reject) => {
            const data = deviceInfo.securityPatchTag;
            console.info('test zyx RNDeviceInfoModule getSecurityPatch data:' + data)
            resolve(deviceInfo.securityPatchTag);
        });
    }

    getSecurityPatchSync(): string {
        console.info('test zyx RNDeviceInfoModule getSecurityPatchSync data:' + deviceInfo.securityPatchTag)
        return deviceInfo.securityPatchTag;
    }

    //只有系统应用可以使用
    getSerialNumber(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getSerialNumberSync());
        });
    }

    getSerialNumberSync(): string {
        return deviceInfo.serial;
    }

    getSystemName(): string {
        return "HarmonyOS";
    }

    getSystemVersion(): string {
        console.info('test zyx RNDeviceInfoModule getSystemVersion data:' + deviceInfo.osReleaseType)
        return deviceInfo.osReleaseType;
    }

    getTags(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getTagsSync();
            console.info('test zyx RNDeviceInfoModule getTags data:' + data)
            resolve(data);
        });
    }

    getTagsSync(): string {
        console.info('test zyx RNDeviceInfoModule getTagsSync data:' + deviceInfo.buildType)
        return deviceInfo.buildType;
    }

    getTotalDiskCapacity(): Promise<number> {
        let path = this.context?.filesDir;
        return statvfs.getTotalSize(path);
    }

    getTotalDiskCapacitySync(): number {
        let path = this.context?.filesDir;
        console.info('test zyx RNDeviceInfoModule getTotalDiskCapacitySync data:' + statvfs.getTotalSizeSync(path))
        return statvfs.getTotalSizeSync(path);
    }

    getTotalDiskCapacityOld(): Promise<number> {
        let path = this.context?.filesDir;
        return statvfs.getTotalSize(path);
    }

    getTotalDiskCapacityOldSync(): number {
        let path = this.context?.filesDir;
        console.info('test zyx RNDeviceInfoModule getTotalDiskCapacityOldSync data:' + statvfs.getTotalSizeSync(path))
        return statvfs.getTotalSizeSync(path);
    }

    getType(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getTypeSync());
        });
    }

    getTypeSync(): string {
        console.info('test zyx RNDeviceInfoModule getTypeSync data:' + deviceInfo.buildType)
        return deviceInfo.buildType;
    }

    getUniqueId(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            resolve(this.getUniqueIdSync());
        });
    }

    //  只有系统应用可以使用
    getUniqueIdSync(): string {
        return deviceInfo.udid
    }

    getUserAgent(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const data = this.getUserAgentSync();
            console.info('test zyx RNDeviceInfoModule getUserAgent data:' + data)
            resolve(data);
        });
    }

    getUserAgentSync(): string {
        let userAgent = '';
        let controller = new web_webview.WebviewController();
        try {
            userAgent = controller.getUserAgent();
            console.log("userAgent: " + userAgent);
        } catch (error) {
            let e: BusinessError = error as BusinessError;
            console.error(`ErrorCode: ${e.code},  Message: ${e.message}`);
        }
        return userAgent;
    }

    getVersion(): string {
        let bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
        let result: string = "";
        try {
            let data = bundleManager.getBundleInfoForSelfSync(bundleFlags);
            console.info('test zyx RNDeviceInfoModule getVersion data:' + JSON.stringify(data))
            if (data) {
                result = data.versionName;
            }
        } catch (err) {
            let message = (err as BusinessError).message;
            console.error(`test zyx getVersion failed error message: ${message}.`);
        }
        return result;
    }

    hasGms(): Promise<boolean> {
        console.info('test zyx RNDeviceInfoModule hasGms')
        return new Promise<boolean>((resolve, reject) => {
            const data = false;
            console.info('test zyx RNDeviceInfoModule hasGms data:' + data)
            resolve(false);
        });
    }

    hasGmsSync(): boolean {
        return false;
    }

    hasHms(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            resolve(true);
        });
    }

    hasHmsSync(): boolean {
        return true;
    }

    isAirplaneMode(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            resolve(this.isAirplaneModeSync());
        });
    }

    // 此接口仅可在FA模型下使用
    isAirplaneModeSync(): boolean {
        let flag: boolean = false;
        let data = settings.getValueSync(this.context, settings.general.AIRPLANE_MODE_STATUS, '1');
        if (data == "1") {
            flag = true;
        }
        return flag;
    }

    isBatteryCharging(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            resolve(this.isBatteryChargingSync());
        });
    }

    isBatteryChargingSync(): boolean {
        let flag = false;
        let chargingStatus = batteryInfo.chargingStatus;

        if (chargingStatus == batteryInfo.BatteryChargeState.NONE ||
            chargingStatus == batteryInfo.BatteryChargeState.DISABLE) {
            flag = false;
        } else if (chargingStatus == batteryInfo.BatteryChargeState.ENABLE) {
            flag = true;
        } else if (chargingStatus == batteryInfo.BatteryChargeState.FULL) {
            flag = true;
        }
        return flag;
    }

    isCameraPresent(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            resolve(this.isCameraPresentSync());
        });
    }

    isCameraPresentSync(): boolean {
        let cameraManager: camera.CameraManager | undefined = undefined;
        try {
            cameraManager = camera.getCameraManager(this.context);
        } catch (error) {
            let err = error as BusinessError;
            console.error(`The getCameraManager call failed. error code: ${err.code}`);
        }
        let cameras: Array<camera.CameraDevice> = [];
        try {
            cameras = cameraManager.getSupportedCameras();
        } catch (error) {
            let err = error as BusinessError;
            console.error(`The getSupportedCameras call failed. error code: ${err.code}`);
        }
        if (cameras && cameras.length > 0) {
            return true;
        }
        return false;
    }

    async isHeadphonesConnected(): Promise<boolean> {
        let audioManager = audio.getAudioManager();
        let audioRoutingManager = audioManager.getRoutingManager();
        let data = await audioRoutingManager.getDevices(audio.DeviceFlag.OUTPUT_DEVICES_FLAG);
        console.info('test zyx isHeadphonesConnected data:' + JSON.stringify(data))
        return null;
    }

    isHeadphonesConnectedSync(): boolean {
        let flag = false;
        // import audio from '@ohos.multimedia.audio';
        // import { BusinessError } from '@ohos.base';
        // import audio from '@ohos.multimedia.audio';
        //
        // let audioManager = audio.getAudioManager();
        // let audioRoutingManager: audio.AudioRoutingManager = audioManager.getRoutingManager();
        //
        // try {
        //   let value: boolean = audioRoutingManager.isCommunicationDeviceActiveSync(audio.CommunicationDeviceType.SPEAKER);
        //   console.info(`Indicate that the active status of the device is obtained ${value}.`);
        // } catch (err) {
        //   let error = err as BusinessError;
        //   console.error(`Failed to obtain the active status of the device ${error}.`);
        // }

        // // DeviceType.BLUETOOTH_A2DP DeviceType.WIRED_HEADPHONES
        // audioManager.isDeviceActive(audio.ActiveDeviceType.BLUETOOTH_A2DP).then((value: boolean) => {
        //   console.info(`Promise returned to indicate that the active status of the device is obtained ${value}.`);
        // });


        // try {
        //   let value: number = audioVolumeGroupManager.getSystemVolumeInDbSync(audio.AudioVolumeType.MEDIA, 3, audio.DeviceType.SPEAKER);
        //   console.info(`Success to get the volume DB. ${value}`);
        // } catch (err) {
        //   let error = err as BusinessError;
        //   console.error(`Fail to adjust the system volume by step. ${error}`);
        // }

        let audioManager = audio.getAudioManager();
        let audioRoutingManager = audioManager.getRoutingManager();
        audioRoutingManager.getDevices(audio.DeviceFlag.OUTPUT_DEVICES_FLAG).then((data) => {
            console.info('test zyx getDevices Promise returned to indicate that the device list is obtained.');
        });
        return flag;
    }

    isWiredHeadphonesConnected(): boolean {
        let flag = false;
        return flag;
    }

    isBluetoothHeadphonesConnected(): boolean {
        let flag = false;
        return flag;
    }

    isLocationEnabled(): Promise<boolean> {
        console.info('test zyx RNDeviceInfoModule isLocationEnabled')
        return new Promise<boolean>((resolve, reject) => {
            const data = geoLocationManager.isLocationEnabled();
            console.info('test zyx RNDeviceInfoModule isLocationEnabled data:' + data)
            resolve(geoLocationManager.isLocationEnabled());
        });
    }

    isLocationEnabledSync(): boolean {
        let locationEnabled = false;
        try {
            locationEnabled = geoLocationManager.isLocationEnabled();
            console.info('test zyx isLocationEnabledSync locationEnabled:' + locationEnabled)
        } catch (err) {
            console.error("errCode:" + (err).code + ",errMessage:" + (err).message);
        }
        return locationEnabled;
    }

    isTablet(): boolean {
        return "tablet" === deviceInfo.deviceType
    }

    supported32BitAbis(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            const data = this.supported32BitAbisSync();
            console.info('test zyx RNDeviceInfoModule supported32BitAbis data:' + data)
            resolve(data);
        });
    }

    supported32BitAbisSync(): string[] {
        let list = this.supportedAbisSync();
        let arr: string[] = [];
        if (list) {
            for (let i = 0; i < list.length; i++) {
                let type = list[i];
                for (let j = 0; j < abiList32.length; j++) {
                    if (type == abiList32[j]) {
                        arr.push(abiList32[j])
                    }
                }
            }
        }
        return arr;
    }

    supported64BitAbis(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            const data = this.supported64BitAbisSync();
            console.info('test zyx RNDeviceInfoModule supported64BitAbis data:' + data)
            resolve(data);
        });
    }

    supported64BitAbisSync(): string[] {
        let list = this.supportedAbisSync();
        let arr: string[] = [];
        if (list) {
            for (let i = 0; i < list.length; i++) {
                let type = list[i];
                for (let j = 0; j < abiList64.length; j++) {
                    if (type == abiList64[j]) {
                        arr.push(abiList64[j])
                    }
                }
            }
        }
        return arr;
    }

    supportedAbis(): Promise<string[]> {
        console.info('test zyx RNDeviceInfoModule supportedAbis')
        return new Promise<string[]>((resolve, reject) => {
            const data = deviceInfo.abiList;
            console.info('test zyx RNDeviceInfoModule supportedAbis data:' + data)
            resolve([deviceInfo.abiList]);
        });
    }

    supportedAbisSync(): string[] {
        return [deviceInfo.abiList];
    }

    getSupportedMediaTypeList(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            const data = this.getSupportedMediaTypeListSync();
            console.info('test zyx RNDeviceInfoModule getSupportedMediaTypeList data:' + data)
            resolve(data);
        });
    }

    getSupportedMediaTypeListSync(): string[] {
        media.CodecMimeType
        let result = [media.CodecMimeType.VIDEO_H263,
            media.CodecMimeType.VIDEO_AVC,
            media.CodecMimeType.VIDEO_MPEG2,
            media.CodecMimeType.VIDEO_MPEG4,
            media.CodecMimeType.VIDEO_VP8,
            media.CodecMimeType.VIDEO_HEVC,
            media.CodecMimeType.AUDIO_AAC,
            media.CodecMimeType.AUDIO_VORBIS,
            media.CodecMimeType.AUDIO_FLAC,
        ];
        return result;
    }
}