import type { RNPackageContext, RNPackage } from 'rnoh/ts';
import { SamplePackage } from 'rnoh-sample-package/ts';
import {SafeAreaViewPackage} from '@react-native-oh-tpl/react-native-safe-area-context/ts';
import { RNDeviceInfoPackage } from "@react-native-oh-tpl/react-native-device-info/ts";

export function createRNPackages(ctx: RNPackageContext): RNPackage[] {
    return [
        new SamplePackage(ctx),
        new SafeAreaViewPackage(ctx),
        new RNDeviceInfoPackage(ctx)];
}
