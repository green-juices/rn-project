#include "RNOH/Package.h"
#include "AMapGeolocationTurboModule.h"
#include "ComponentDescriptors.h"

using namespace rnoh;
using namespace facebook;

class AMapGeolocationTurboModuleFactoryDelegate : public TurboModuleFactoryDelegate {
public:
    SharedTurboModule createTurboModule(Context ctx, const std::string &name) const override {
        if (name == "AMapGeolocation") {
            return std::make_shared<AMapGeolocationTurboModule>(ctx, name);
        }
        return nullptr;
    };
};
namespace rnoh {

    class AMapGeolocationPackage : public Package {
    public:
        AMapGeolocationPackage(Package::Context ctx) : Package(ctx) {}

        std::unique_ptr<TurboModuleFactoryDelegate> createTurboModuleFactoryDelegate() override {
            return std::make_unique<AMapGeolocationTurboModuleFactoryDelegate>();
        }

        std::vector<facebook::react::ComponentDescriptorProvider> createComponentDescriptorProviders() override {
            return {
                facebook::react::concreteComponentDescriptorProvider<
                    facebook::react::AMapGeolocationComponentDescriptor>(),
            };
        }
      
    };
} // namespace rnoh
