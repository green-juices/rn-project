#pragma once
#include "RNOH/ArkTSTurboModule.h"
#include <ReactCommon/TurboModule.h>


namespace rnoh {
    class JSI_EXPORT AMapGeolocationTurboModule : public ArkTSTurboModule {
    public:
        AMapGeolocationTurboModule(const ArkTSTurboModule::Context ctx, const std::string name);
    };
} // namespace rnoh