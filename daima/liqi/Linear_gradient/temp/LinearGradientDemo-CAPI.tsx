import React, {useRef} from 'react';
import {ScrollView, StyleSheet, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
function LinearGradientDemo() {
    let colorArr: String[] = ["red","gree","blue"];
    let clickIndex:number = 0;
    const [backColor, setLinerGradientColor] = React.useState(colorArr[clickIndex]);
    const backColorRef = useRef(backColor)
    const handleClick = () => {
        clickIndex = clickIndex + 1;
        setLinerGradientColor(backColor => colorArr[clickIndex % 3])
        console.log("value3: ", backColorRef)
        setTimeout(() => {
            console.log(backColorRef.current)
        }, 0)
    }
  const scrollRef = React.useRef<ScrollView>(null);
  return (
    <ScrollView style={styles.container} ref={scrollRef}>
      <Text style={styles.footer}>colors={['green', 'blue', 'red']}</Text>
      <Text style={styles.footer}>start:[0,0],end:[1,0]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['#d02828', '#832020', '#000000']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        style={styles.gradient}>
        <Text style={styles.buttonText}>子组件1</Text>
      </LinearGradient>

      <Text style={styles.footer}>start:[1,0],end:[0,0]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['green', 'blue', 'red']}
        start={{x: 1, y: 0}}
        end={{x: 0, y: 0}}
        onClick={() => {
                  Alert.alert('Test Alert', 'Message', [
                    {text: 'OK', onPress: () => setState(true)},
                  ]);
                }}
        style={styles.gradient}
      />

      <Text style={styles.footer}>start:[0,0],end:[0,1]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['green', 'blue', 'red']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        style={styles.gradient}
      />

      <Text style={styles.footer}>start:[0,1],end:[0,0]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['green', 'blue', 'red']}
        start={{x: 0, y: 1}}
        end={{x: 0, y: 0}}
        style={styles.gradient}
      />

      <Text style={styles.footer}>start:[0,0],end:[1,1]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['green', 'blue', 'red']}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.gradient}
      />

      <Text style={styles.footer}>start:[1,0],end:[0,1]</Text>
      <LinearGradient
        angleCenter={{x: 0.5, y: 0.5}}
        colors={['green', 'blue', 'red']}
        start={{x: 1, y: 0}}
        end={{x: 0, y: 1}}
        style={styles.gradient}
      />
      <Text style={styles.footer}>angle:30</Text>
      <LinearGradient
        angle={30}
        useAngle={true}
        colors={['green', 'blue', 'red']}
        locations={[0, 0.5, 1]}
        angleCenter={{x: 0.5, y: 0.5}}
        style={styles.gradient}
      />
      <Text style={styles.footer}>angle:45</Text>
      <LinearGradient
        angle={45}
        useAngle={true}
        colors={['green', 'blue', 'red']}
        locations={[0, 0.5, 1]}
        angleCenter={{x: 0.5, y: 0.5}}
        style={styles.gradient}
      />

      <Text style={styles.footer}>angle:120</Text>
      <LinearGradient
        angle={120}
        useAngle={true}
        colors={['green', 'blue', 'red']}
        angleCenter={{x: 0.5, y: 0.5}}
        locations={[0, 0.5, 1]}
        style={styles.gradient}
      />
      <Text style={styles.footer}>facebook button</Text>
      <LinearGradient
        colors={['#4c669f', '#3b5998', '#192f6a']}
        style={styles.gradientFB}>
        <Text style={styles.buttonTextFB}>Sign in with Facebook</Text>
      </LinearGradient>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  gradient: {
    width: 60,
    height: 60,
  },
  gradientFB: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
  },
  footer: {
    height: 40,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#333',
  },
  buttonText: {
    width: 50,
    height: 50,
    fontWeight: 'bold',
  },
  buttonTextFB: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

export default LinearGradientDemo;
