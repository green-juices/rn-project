import React from 'react';
import {ScrollView, StyleSheet, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Effect} from "../components";
import {TestSuite, TestCase} from '@rnoh/testerino';

export function LinearGradientTest() {
return (
    <>
        <TestSuite name="LinearGradient.Value">
            <TestCase>
                <LinearGradient
                    angleCenter={{x: 0.5, y: 0.5}}
                    colors={['#d02828', '#832020', '#000000']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    style={styles.gradient} />
            </TestCase>

            <TestCase itShould="The rotation center is the center point of the shape">
                <LinearGradient angleCenter={{x: 0.5, y: 0.5}} />
            </TestCase>
            <TestCase itShould="The gradient color is green, blue, and red">
                <LinearGradient colors={['green', 'blue', 'red']} />
            </TestCase>
            <TestCase itShould="The starting position of the gradient color">
                <LinearGradient start={{x: 0, y: 0}} />
            </TestCase>
            <TestCase itShould="The ending position of the gradient color">
                <LinearGradient end={{x: 1, y: 0}} />
            </TestCase>
            <TestCase
                itShould="The Properties of Gradient Shapes"
                fn={({expect}) => {
                      expect(LinearGradient.height).to.be.eq(60);
                      expect(LinearGradient.width).to.be.eq(60);
                  }}>
                <LinearGradient style={{width: 60, height: 60}} />
            </TestCase>
            <TestCase
                itShould="The Properties of Gradient Shapes"
                fn={({expect}) => {
                    expect(LinearGradient.height).to.be.eq(100);
                    expect(LinearGradient.width).to.be.eq(100);
                }}>
                <LinearGradient style={{width: 100, height: 100}} />
            </TestCase>
            <TestCase itShould="The Properties of Gradient Shapes">
                <LinearGradient style={{width: 60, height: 60}} >
                    <Text style={{width: 50, height: 50, fontWeight: 'bold',}}>Show Text</Text>
                </LinearGradient>
            </TestCase>
        </TestSuite>

    </>
);
}