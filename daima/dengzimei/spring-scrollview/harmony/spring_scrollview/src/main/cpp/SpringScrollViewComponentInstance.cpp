#include "SpringScrollViewComponentInstance.h"
#include "Types.h"
#include <folly/dynamic.h>
#include "RNOH/arkui/ArkUINode.h"
#include "RNOH/arkui/ScrollNode.h"
#include "RNOHCorePackage/ComponentInstances/ScrollViewComponentInstance.h"
#include <arkui/native_interface.h>
#include <arkui/native_node.h>
#include "RNOH/arkui/NativeNodeApi.h"
#include <arkui/native_node.h>
#include <arkui/native_gesture.h>
namespace rnoh {
SpringScrollViewComponentInstance::SpringScrollViewComponentInstance(Context context)
    : CppComponentInstance(std::move(context)) {
    LOG(INFO) << "insertChild1";
    m_springStackNode.insertChild(m_headerStackNode, 0);
    m_springStackNode.insertChild(m_scrollViewNode, 1);
    m_springStackNode.insertChild(m_footerStackNode, 2);
    m_springStackNode.setSpringScrollViewNodeDelegate(this);
    LOG(INFO) << "insertChild";
//         ArkUI_NumberValue clipValue[] = {{.u32 = 1}};
//         ArkUI_AttributeItem clipItem = {clipValue, sizeof(clipValue) / sizeof(ArkUI_NumberValue)};
//         NativeNodeApi::getInstance()->setAttribute(m_headerStackNode.getArkUINodeHandle(), NODE_CLIP, &clipItem);
    panGesture(m_springStackNode.getArkUINodeHandle());
}
void SpringScrollViewComponentInstance::panGesture(ArkUI_NodeHandle arkUI_NodeHandle) {
    auto anyGestureApi = OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_GESTURE, "ArkUI_NativeGestureAPI_1");
    auto gestureApi = reinterpret_cast<ArkUI_NativeGestureAPI_1 *>(anyGestureApi);
    auto panGesture = gestureApi->createPanGesture(1, GESTURE_DIRECTION_VERTICAL, 5);
    auto onPanActionCallBack = [](ArkUI_GestureEvent *event, void *extraParam) {
        SpringScrollViewComponentInstance *instance = (SpringScrollViewComponentInstance *)extraParam;
        if (!instance->m_springStackNode.getSpringScrollViewConfigurator().getHasRefresh()) {
            return;
        }
        ArkUI_GestureEventActionType actionType = OH_ArkUI_GestureEvent_GetActionType(event);
        if (actionType == GESTURE_EVENT_ACTION_ACCEPT) {
            instance->offsetY = 0;
            instance->downY = OH_ArkUI_PanGesture_GetOffsetY(event);
            instance->touchYOld = instance->offsetY;
            //instance->beginRefresh();
        } else if (actionType == GESTURE_EVENT_ACTION_UPDATE) {
            instance->offsetY = OH_ArkUI_PanGesture_GetOffsetY(event) - instance->downY;
            if (instance->offsetY >= 0) {
                instance->onActionUpdate();
            } else {
                instance->onActionEnd();
            }
        } else if (actionType == GESTURE_EVENT_ACTION_END) {
            instance->onActionEnd();
            instance->endRefresh();
        }
    };
    gestureApi->setGestureEventTarget(
        panGesture, GESTURE_EVENT_ACTION_ACCEPT | GESTURE_EVENT_ACTION_UPDATE | GESTURE_EVENT_ACTION_END, this,
        onPanActionCallBack);
    gestureApi->addGestureToNode(arkUI_NodeHandle, panGesture, PARALLEL, NORMAL_GESTURE_MASK);
}
void SpringScrollViewComponentInstance::onActionUpdate() {
    if (state == IS_FREE || state == IS_PULL_DOWN_1 || state == IS_PULL_DOWN_2 || state == IS_PULL_UP_1 ||
        state == IS_PULL_UP_2) {
        auto maxTranslate = m_springStackNode.getSpringScrollViewConfigurator().getMaxTranslate();
        auto loadImgHeight = m_springStackNode.getSpringScrollViewConfigurator().getLoadImgHeight();
        auto refreshHeight = m_springStackNode.getSpringScrollViewConfigurator().getRefreshHeight();
        touchYNew = offsetY;
        if (!isComponentTop()) {
            return;
        }
        auto isPullAction = (touchYNew - touchYOld) > 0;
        if ((state == IS_FREE && isPullAction) || state == IS_PULL_DOWN_1 || state == IS_PULL_DOWN_2) {
            if (m_springStackNode.getSpringScrollViewConfigurator().getHasRefresh()) {
                // ��ȡ����λ�ƾ���
                float trY = touchYNew - touchYOld;
                // ���㵱ǰ��Ҫλ�Ƶľ���
                trYTop = this->getTranslateYOfRefresh(trY);
               // setRefreshHeaderHeight(trYTop);
                if (trYTop / maxTranslate < 0.5) {
                    state = IS_PULL_DOWN_1;
                } else {
                    state = IS_PULL_DOWN_2;
                    // ���ͷ�ˢ��ʱ����
                    //this->beginRefresh();
                }
                if (trY > 0) {
                    this->onScroll(trYTop);
                }
            }
        }
        touchYOld = touchYNew;
    }
}

void SpringScrollViewComponentInstance::onActionEnd() {
    auto maxTranslate = m_springStackNode.getSpringScrollViewConfigurator().getMaxTranslate();
    auto refreshAnimDuration = m_springStackNode.getSpringScrollViewConfigurator().getRefreshAnimDuration();
    if (trYTop > 0) {
        if (state == IS_FREE || state == IS_PULL_DOWN_1 || state == IS_PULL_DOWN_2) {
            if (trYTop / maxTranslate < 0.5) {
                //closeRefresh(trYTop, 0, m_springStackNode.getSpringScrollViewConfigurator().getAnimDuration());
            } else {
                state = IS_REFRESHING;
                trYTop = maxTranslate * 0.5; // ���λ��Ҫ���ø߶ȣ��ջ�ȥ
                this->onRefresh();
                //setPullHeaderHeight(trYTop);
                //setRefreshHeaderHeight(trYTop);
            }
        }
    }
}
float SpringScrollViewComponentInstance::getTranslateYOfRefresh(float newTranslateY) {
    auto config = m_springStackNode.getSpringScrollViewConfigurator();
    if (&config != nullptr) {
        facebook::react::Float maxTranslateY = config.getMaxTranslate();
        facebook::react::Float sensitivity = config.getSensitivity();
        // ����ֵ����
        facebook::react::Float dampingFactor;
        if ((trYTop / maxTranslateY) < 0.2) {
            dampingFactor = 1.0;
        } else if ((trYTop / maxTranslateY) < 0.4) {
            dampingFactor = 0.8;
        } else if ((trYTop / maxTranslateY) < 0.6) {
            dampingFactor = 0.6;
        } else if ((trYTop / maxTranslateY) < 0.8) {
            dampingFactor = 0.4;
        } else {
            dampingFactor = 0.2;
        }
        newTranslateY = newTranslateY * dampingFactor * sensitivity;
        // ����ֵ����
        facebook::react::Float newTotalTranslateY = trYTop + newTranslateY;
        if (newTotalTranslateY > maxTranslateY) {
            return maxTranslateY;
        } else if (newTotalTranslateY < 0) {
            return 0;
        } else {
            return newTotalTranslateY;
        }
    }
    return 0;
}

// void SpringScrollViewComponentInstance::closeRefresh(float start, float target, int32_t duration) {
//     if (trYTop == target) {
//         return;
//     }
//     if (animation != nullptr &&
//         (animation->GetAnimationStatus() == ANIMATION_START || animation->GetAnimationStatus() == ANIMATION_RUN)) {
//         return;
//     }
//     if (animation == nullptr) {
//         animation = new Animation();
//     }
//     animation->SetAnimationParams(static_cast<std::chrono::milliseconds>(duration), start, target,
//                                   [this, &target](double value) {
//                                       trYTop = value < 0 ? 0 : value;
//                                       //setPullHeaderHeight(trYTop);
//                                       if (trYTop == 0) {
//                                           state = IS_FREE;
//                                           m_springStackNode.markDirty();
//                                       }
//                                       //onMomentumScrollEnd(trYTop);
//                                   });
//     if (animation->GetAnimationStatus() == ANIMATION_FREE || animation->GetAnimationStatus() == ANIMATION_FINISH) {
//         animation->Start();
//     }
// }

void SpringScrollViewComponentInstance::endRefresh() {
    if (refreshStatus != "refreshing") {
        return;
    }
    refreshStatus = "rebound";
}
void SpringScrollViewComponentInstance::onChildInserted(ComponentInstance::Shared const &childComponentInstance,
                                                        std::size_t index) {
    CppComponentInstance::onChildInserted(childComponentInstance, index);
    if (!isHeaderInserted) {
        LOG(INFO) << "headerInserted";
        m_headerStackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
        isHeaderInserted = true;
    } else if (!isFooterInserted) {
        LOG(INFO) << "FooterInserted";
        m_footerStackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
        isFooterInserted = true;
    } else {
        LOG(INFO) << "ScrollInserted";
        m_springStackNode.insertChild(childComponentInstance->getLocalRootArkUINode(), index);
    }
    auto nodeValue = NativeNodeApi::getInstance()->getAttribute(m_springStackNode.getArkUINodeHandle(), NODE_WIDTH);
    initialContentOffset.x = nodeValue->value[nodeValue->size - 1].f32;
    m_headerStackNode.setSize(facebook::react::Size({initialContentOffset.x, 0}));
}
bool SpringScrollViewComponentInstance::isComponentTop() {
    LOG(INFO) << "ScrollView0";
    std::vector<ComponentInstance::Shared> child = getChildren();
    for (ComponentInstance::Shared c : child) {
        LOG(INFO) << "ScrollView1";
        if (c->getComponentName() == "ScrollView") {
            auto scrollView = std::dynamic_pointer_cast<rnoh::ScrollViewComponentInstance>(c);
            if (scrollView != nullptr) {
                return scrollView->getScrollViewMetrics().contentOffset.y <= 0;
            }
        }
    }
    return false;
};

void SpringScrollViewComponentInstance::onNativeResponderBlockChange(bool blocked) {
    LOG(INFO) << "ScrollView2";
    std::vector<ComponentInstance::Shared> child = getChildren();
    for (ComponentInstance::Shared c : child) {
        LOG(INFO) << "ScrollView3";
        if (c->getComponentName() == "ScrollView") {
            auto scrollView = std::dynamic_pointer_cast<rnoh::ScrollViewComponentInstance>(c);
            if (blocked) {
                scrollView->setNativeResponderBlocked(!blocked, "REACT_NATIVE");
            }
            break;
        }
    }
}
void SpringScrollViewComponentInstance::onChildRemoved(ComponentInstance::Shared const &childComponentInstance) {
    CppComponentInstance::onChildRemoved(childComponentInstance);
    LOG(INFO) << "ChildRemoved";
    m_headerStackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
    m_springStackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
    m_footerStackNode.removeChild(childComponentInstance->getLocalRootArkUINode());
};

    SpringScrollViewNode &SpringScrollViewComponentInstance::getLocalRootArkUINode() { return m_springStackNode; }

void SpringScrollViewComponentInstance::onPropsChanged(SharedConcreteProps const &props) {
    LOG(INFO) << "SpringScrollViewProps";
    CppComponentInstance::onPropsChanged(props);
    if (props == nullptr) {
        return;
    }
    if (auto p = std::dynamic_pointer_cast<const facebook::react::RNCSpringScrollViewProps>(props)) {
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> refreshHeaderHeight: "
                     << p->refreshHeaderHeight;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> decelerationRate: " << p->decelerationRate;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> loadingFooterHeight: "
                     << p->loadingFooterHeight;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> bounces: " << p->bounces;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> allLoaded: " << p->allLoaded;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> inverted: " << p->inverted;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> scrollEnabled: " << p->scrollEnabled;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> pagingEnabled: " << p->pagingEnabled;
        LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> directionalLockEnabled: "
                     << p->directionalLockEnabled;

            LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> initialContentOffset: "
                         << p->initialContentOffset.x + ':' + p->initialContentOffset.y;
            LOG(WARNING) << "[clx] <SpringScrollViewComponentInstance::setProps> pageSize: "
                         << p->pageSize.width + ':' + p->pageSize.height;



            this->getLocalRootArkUINode().setInitialContentOffset(p->initialContentOffset.x, p->initialContentOffset.y);
            this->getLocalRootArkUINode().setRefreshHeaderHeight(p->refreshHeaderHeight);
            this->getLocalRootArkUINode().setLoadingFooterHeight(p->loadingFooterHeight);
            this->getLocalRootArkUINode().setDecelerationRate(p->decelerationRate);
            this->getLocalRootArkUINode().setBounces(p->bounces);
            this->getLocalRootArkUINode().setAllLoaded(p->allLoaded);
            this->getLocalRootArkUINode().setInverted(p->inverted);
            this->getLocalRootArkUINode().setScrollEnabled(p->scrollEnabled);
            this->getLocalRootArkUINode().setPagingEnabled(p->pagingEnabled);
            this->getLocalRootArkUINode().setPageSize(p->pageSize.width, p->pageSize.height);
            this->getLocalRootArkUINode().setDirectionalLockEnabled(p->directionalLockEnabled);
        }
    }

void SpringScrollViewComponentInstance::onRefresh() {
    LOG(INFO) << "onRefresh";
    m_eventEmitter->onRefresh({});
};
void SpringScrollViewComponentInstance::onLoading() {
    LOG(INFO) << "onLoading";
    m_eventEmitter->onLoading({});
};
void SpringScrollViewComponentInstance::onTouchBegin() {
    LOG(INFO) << "onTouchBegin";
    m_eventEmitter->onTouchBegin({});
};
void SpringScrollViewComponentInstance::onTouchEnd() {
    LOG(INFO) << "onTouchEnd";
    m_eventEmitter->onTouchEnd({});
};
void SpringScrollViewComponentInstance::onMomentumScrollBegin() {
    LOG(INFO) << "onMomentumScrollBegin";
    m_eventEmitter->onMomentumScrollBegin({});
};
void SpringScrollViewComponentInstance::onMomentumScrollEnd() { m_eventEmitter->onMomentumScrollEnd({}); };
void SpringScrollViewComponentInstance::onScroll(const float &displayedScrollHeight) {
    LOG(INFO) << "onScroll";
    facebook::react::Float x = displayedScrollHeight;
    m_eventEmitter->onScroll({displayedScrollHeight, y});
};
void SpringScrollViewComponentInstance::onSizeChange(const float &displayedScrollHeight) {
    LOG(INFO) << "onSizeChange";
    facebook::react::Float width = displayedScrollHeight;
    m_eventEmitter->onSizeChange({displayedScrollHeight, height});
};
void SpringScrollViewComponentInstance::onContentSizeChange(const float &displayedScrollHeight) {
    LOG(INFO) << "onContentSizeChange";
    facebook::react::Float width = displayedScrollHeight;
    m_eventEmitter->onContentSizeChange({displayedScrollHeight, height});
};

    void SpringScrollViewComponentInstance::getNapiProps(facebook::react::Props::Shared props) {
        auto p = std::dynamic_pointer_cast<const facebook::react::RNCSpringScrollViewProps>(props);
        refreshHeaderHeight = p->refreshHeaderHeight;
        decelerationRate = p->decelerationRate;
        loadingFooterHeight = p->loadingFooterHeight;
        bounces = p->bounces;
        inverted = p->inverted;
        allLoaded = p->allLoaded;
        scrollEnabled = p->scrollEnabled;
        pagingEnabled = p->pagingEnabled;
        initialContentOffset.x = p->initialContentOffset.x;
        initialContentOffset.y = p->initialContentOffset.y;
        pageSize.width = p->pageSize.width;
        pageSize.height = p->pageSize.height;
    }
} // namespace rnoh