#ifndef HARMONY_TYPES_H
#define HARMONY_TYPES_H
namespace Types{
    struct Offset {
        float x;
        float y;

        Offset(float xVal = 0.0f, float yVal = 0.0f);
    };

    struct EdgeInsets {
        float top;
        float bottom;
        float left;
        float right;

        EdgeInsets(float t = 0.0f, float b = 0.0f, float l = 0.0f, float r = 0.0f);
    };

    struct Size {
        float width;
        float height;

        Size(float w = 0.0f, float h = 0.0f);
    };

    struct Point {
        float x;
        float y;

        Point(float xVal = 0.0f, float yVal = 0.0f);
    };

};


#endif