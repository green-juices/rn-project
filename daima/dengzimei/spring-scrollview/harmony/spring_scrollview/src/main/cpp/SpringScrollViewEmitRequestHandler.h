/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef SPRING_SRC_MAIN_CPP_RNCSpringScrollViewEventEmitter_H
#define SPRING_SRC_MAIN_CPP_RNCSpringScrollViewEventEmitter_H

#include <glog/logging.h>
#include "glog/logging.h"
#include "EventEmitters.h"
#include "RNOH/ArkJS.h"
#include "RNOH/EventEmitRequestHandler.h"

namespace rnoh {

    class SpringScrollViewEmitRequestHandler : public EventEmitRequestHandler {
    public:
        void handleEvent(EventEmitRequestHandler::Context const &ctx) override {
            ArkJS arkJs(ctx.env);
            auto eventName = ctx.eventName;
            auto eventEmitter =
                ctx.shadowViewRegistry->getEventEmitter<facebook::react::RNCSpringScrollViewEventEmitter>(ctx.tag);
            if (eventEmitter == nullptr) {
                return;
            }
            if (eventName == "onRefresh") {
                eventEmitter->onRefresh({});
            } else if (eventName == "onLoading") {
                eventEmitter->onLoading({});
            } else if (eventName == "onTouchBegin") {;
                eventEmitter->onTouchBegin({});
            } else if (eventName == "onTouchEnd") {
                eventEmitter->onTouchEnd({});
            } else if (eventName == "onMomentumScrollBegin") {
                eventEmitter->onMomentumScrollBegin({});
            } else if (eventName == "onMomentumScrollEnd") {
                eventEmitter->onMomentumScrollEnd({});
            } else if (eventName == "onScroll") {
                eventEmitter->onScroll({});
            }else if (eventName == "onSizeChange") {
                eventEmitter->onSizeChange({});
            }else if (eventName == "onContentSizeChange") {
                eventEmitter->onContentSizeChange({});
            }
        }
    };

} // namespace rnoh
#endif