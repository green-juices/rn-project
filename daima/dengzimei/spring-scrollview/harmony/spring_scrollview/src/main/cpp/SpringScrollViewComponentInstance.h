//
// Created on 4/3/2024.
//
// Node APIs are not fully supported. To solve the compilation error of the interface cannot be found,
// please include "napi/native_api.h".

#ifndef HARMONY_SPRINGSCROLLVIEWCOMPONENTINSTANCE_H
#define HARMONY_SPRINGSCROLLVIEWCOMPONENTINSTANCE_H

#include "Animation.h"
#include "RNOH/CppComponentInstance.h"
#include "RNOH/arkui/ScrollNode.h"
#include "SpringScrollViewNode.h"
#include "Props.h"
#include "ShadowNodes.h"
#include "Types.h"
#include "SpringScrollState.h"
#include <future>

namespace rnoh {
class SpringScrollViewComponentInstance : public CppComponentInstance<facebook::react::RNCSpringScrollViewShadowNode>,
                                          public SpringScrollViewNodeDelegate {
private:
    SpringScrollViewNode m_springStackNode;
    StackNode m_headerStackNode;
    ScrollNode m_scrollViewNode;
    StackNode m_footerStackNode;
    std::shared_ptr<const facebook::react::RNCSpringScrollViewEventEmitter> m_smartRefreshLayoutEventEmitter;

        Types::Offset initialContentOffset;
        Types::Size pageSize;

        float refreshHeaderHeight = 0.0f;
        float loadingFooterHeight;
        float decelerationRate;
        bool bounces;
        bool scrollEnabled;
        bool inverted;
        bool allLoaded;
        bool directionalLockEnabled;
        bool pagingEnabled;

    facebook::react::Float y{0.0};
    facebook::react::Float height{0.0};
    bool isHeaderInserted{false};
    bool isFooterInserted{false};
    float trYTop{0.0};
    float mWidth{0.0};
    int32_t state{IS_FREE};
    int32_t touchYOld{0};
    int32_t touchYNew{0};
    int32_t downY{0};   // first down touch on Y
    int32_t offsetY{0}; // pan offset on Y
    Animation *animation{nullptr};

    std::string refreshStatus = "waiting";
    std::string loadingStatus = "waiting";
    std::string draggingDirection;

    float maxHeight;
    float bottomOffset;
	public:
    SpringScrollViewComponentInstance(Context context);
    void onChildInserted(ComponentInstance::Shared const &childComponentInstance, std::size_t index) override;
    void onChildRemoved(ComponentInstance::Shared const &childComponentInstance) override;
    SpringScrollViewNode &getLocalRootArkUINode() override;
    void onPropsChanged(SharedConcreteProps const &props) override;
    void getNapiProps(facebook::react::Props::Shared props);

    //void setRefreshHeaderHeight(float height);
 
    //gesture
    void panGesture(ArkUI_NodeHandle arkUI_NodeHandle);
    void onActionUpdate();
    void onActionEnd();
    float getTranslateYOfRefresh(float newTranslateY);
    bool isComponentTop() override;
    void onNativeResponderBlockChange(bool isBlocked) override;
    void closeRefresh(float start, float target, int32_t duration);

//scroll
    std::promise<void> scrollTo(float x, float y, bool animated);
    std::promise<void> scroll(float x, float y, bool animated);
    std::promise<void> beginRefresh();
    std::promise<void> scrollToEnd(bool animated);
    std::promise<void> scrollToBegin(bool animated);
    void endRefresh();
    void endLoading(bool rebound);
//event
    void onRefresh() override;
    void onLoading() override;
    void onTouchBegin() override;
    void onTouchEnd() override;
    void onMomentumScrollBegin() override;
    void onMomentumScrollEnd() override;
    void onScroll(const float &displayedScrollHeight) override;
    void onSizeChange(const float &displayedScrollHeight) override;
    void onContentSizeChange(const float &displayedScrollHeight) override;

};

} // namespace rnoh

#endif // HARMONY_SpringScrollViewCOMPONENTINSTANCE_H
