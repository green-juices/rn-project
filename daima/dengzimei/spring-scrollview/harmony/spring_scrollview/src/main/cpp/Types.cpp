#include "Types.h"
namespace Types {

    Offset::Offset(float xVal, float yVal) : x(xVal), y(yVal) {}

    EdgeInsets::EdgeInsets(float t, float b, float l, float r) : top(t), bottom(b), left(l), right(r) {}

    Size::Size(float w, float h) : width(w), height(h) {}

    Point::Point(float xVal, float yVal) : x(xVal), y(yVal) {}

} 
