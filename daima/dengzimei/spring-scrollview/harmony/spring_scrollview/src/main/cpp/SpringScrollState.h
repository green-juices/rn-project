#include <string>
enum { IS_FREE, IS_PULL_DOWN_1, IS_PULL_DOWN_2, IS_REFRESHING, IS_REFRESHED, IS_PULL_UP_1, IS_PULL_UP_2, IS_LOADING };
const std::string HeaderStatus[] = {"waiting", "pulling", "pullingEnough", "pullingCancel", "refreshing", "rebound"};
const std::string FooterStatus[] = {"waiting",        "dragging", "draggingEnough", "draggingCancel",
                                    "releaseRebound", "loading",  "rebound",        "allLoaded"};