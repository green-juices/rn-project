/**
 * @format
 */

// import {AppRegistry} from 'react-native';
// import App from './App';
// import {name as appName} from './app.json';

// AppRegistry.registerComponent(appName, () => App);


import React from "react";
import {requireNativeComponent} from "react-native";

const SpringScrollViewNative = requireNativeComponent("RNCSpringScrollView");
export default SpringScrollView = SpringScrollViewNative;
