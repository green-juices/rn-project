// @ts-check
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {AppParamsContext} from './contexts';
// @ts-expect-error
import ReactNativeFeatureFlags from 'react-native/Libraries/ReactNative/ReactNativeFeatureFlags';
import {
  AnimationsExample,
  CheckerboardExample,
  ChessboardExample,
  CursorExample,
  FlatListVsScrollViewExample,
  ImageGalleryExample,
  LargeImageScrollExample,
  StickyHeadersExample,
  TesterExample,
  TextScrollExample,
  TogglingComponentExample,
} from './examples';
import { TextTestsExample } from './examples/TestTextsExample';

import LinearGradientDemo from './third_party_demo/LinearGradientDemo/LinearGradientDemo-CAPI';
import FlashListDemo from './third_party_demo/FlashListDemo/App';
import PagerViewDemo from './third_party_demo/PagerViewDemo-CAPI';
import SafeAreaDemo from './third_party_demo/SafeAreaDemo-CAPI';
import SmartRefreshLayoutDemo from './third_party_demo/SmartRefreshLayoutDemo/SmartRefreshLayoutDemo-CAPI';
import MaskedViewDemo from './third_party_demo/MaskedViewDemo-CAPI';
import FastImageDemo from './third_party_demo/FastImageDemo-CAPI';
import RNFastImageDemo from './third_party_demo/RNFastImageDemo';
import WebViewDemo from './third_party_demo/WebViewDemo-CAPI';
import DatetimePickerDemo from './third_party_demo/DatetimePickerDemo-CAPI';
import LottieDemo from './third_party_demo/LottieDemo-CAPI';
import SVGDemo from './third_party_demo/SVGDemo-CAPI';
import VideoDemo from './third_party_demo/VideoDemo-CAPI';
import TabViewDemo from './third_party_demo/TabViewDemo';
import GestureHandlerDemo from './third_party_demo/GestureHandlerDemo/App';
 import CardViewDemo from './third_party_demo/CardViewDemo/CardVIewDemo-CAPI';

AppRegistry.setWrapperComponentProvider(appParams => {
  return ({children, ..._otherProps}) => (
    <AppParamsContext.Provider value={appParams}>
      {children}
    </AppParamsContext.Provider>
  );
});

ReactNativeFeatureFlags.shouldEmitW3CPointerEvents = () => true;
ReactNativeFeatureFlags.shouldPressibilityUseW3CPointerEventsForHover = () =>
  true;

AppRegistry.registerComponent(appName, () => App);
// AppRegistry.registerComponent(appName, () => LinearGradientDemo);
// AppRegistry.registerComponent(appName, () => FlashListDemo);
// AppRegistry.registerComponent(appName, () => PagerViewDemo);
// AppRegistry.registerComponent(appName, () => SafeAreaDemo);
// AppRegistry.registerComponent(appName, () => SmartRefreshLayoutDemo);
// AppRegistry.registerComponent(appName, () => MaskedViewDemo);
// AppRegistry.registerComponent(appName, () => FastImageDemo);
// AppRegistry.registerComponent(appName, () => WebViewDemo);
// AppRegistry.registerComponent(appName, () => DatetimePickerDemo);
// AppRegistry.registerComponent(appName, () => LottieDemo);
// AppRegistry.registerComponent(appName, () => SVGDemo);
// AppRegistry.registerComponent(appName, () => VideoDemo);
// AppRegistry.registerComponent(appName, () => TabViewDemo);
// AppRegistry.registerComponent(appName, () => GestureHandlerDemo);
// AppRegistry.registerComponent(appName, () => ViewShotDemo);
// AppRegistry.registerComponent(appName, () => RNFastImageDemo);
 AppRegistry.registerComponent(appName, () => CardViewDemo);


AppRegistry.registerComponent('tester', () => TesterExample);
AppRegistry.registerComponent('animations', () => AnimationsExample);
AppRegistry.registerComponent('text_tests', () => TextTestsExample);
AppRegistry.registerComponent('checkerboard', () => CheckerboardExample);
AppRegistry.registerComponent('chessboard', () => ChessboardExample);
AppRegistry.registerComponent('cursor', () => CursorExample);
AppRegistry.registerComponent('image_gallery', () => ImageGalleryExample);
AppRegistry.registerComponent(
  'large_image_scroll',
  () => LargeImageScrollExample,
);
AppRegistry.registerComponent('text_scroll', () => TextScrollExample);
AppRegistry.registerComponent('flat_list', () => FlatListVsScrollViewExample);
AppRegistry.registerComponent('toggling', () => TogglingComponentExample);
AppRegistry.registerComponent('sticky_headers', () => StickyHeadersExample);
